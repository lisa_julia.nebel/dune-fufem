// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_IMPROVEGRID_HH
#define DUNE_FUFEM_IMPROVEGRID_HH

#include <cmath>
#include <vector>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/version.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/matrix-vector/crossproduct.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/prolongboundarypatch.hh>

template <class EntityType>
double aspectRatio(const EntityType& entity)
{
    if (!entity.type().isTetrahedron())
        DUNE_THROW(Dune::NotImplemented, "aspect ratio only for simplices!");

    const int dim = EntityType::dimension;

    namespace MV = Dune::MatrixVector;
    typename EntityType::Geometry geometry = entity.geometry();

    const Dune::FieldVector<double,dim> a = geometry.corner(0);
    const Dune::FieldVector<double,dim> b = geometry.corner(1);
    const Dune::FieldVector<double,dim> c = geometry.corner(2);
    const Dune::FieldVector<double,dim> d = geometry.corner(3);

    Dune::FieldVector<double,dim> ba = b - a;
    Dune::FieldVector<double,dim> ca = c - a;
    Dune::FieldVector<double,dim> da = d - a;

    Dune::FieldVector<double,dim> circumRadiusNumerator0 = MV::crossProduct(ba,ca);
    circumRadiusNumerator0 *= da.two_norm2();
    Dune::FieldVector<double,dim> circumRadiusNumerator1 = MV::crossProduct(da,ba);
    circumRadiusNumerator1 *= ca.two_norm2();
    Dune::FieldVector<double,dim> circumRadiusNumerator2 = MV::crossProduct(ca,da);
    circumRadiusNumerator2 *= ba.two_norm2();

    Dune::FieldVector<double,dim> circumRadiusNumerator = circumRadiusNumerator0
        + circumRadiusNumerator1 + circumRadiusNumerator2;

    double circumRadius = circumRadiusNumerator.two_norm() / (2*(ba*MV::crossProduct(ca,da)));

    double volume = MV::crossProduct(ba,ca) * da / 6;

    double area = MV::crossProduct(ba,ca).two_norm()/2;
    area += MV::crossProduct(da,ba).two_norm()/2;
    area += MV::crossProduct(da,ca).two_norm()/2;
    area += MV::crossProduct(Dune::FieldVector<double,dim>(c-b),Dune::FieldVector<double,dim>(d-b)).two_norm()/2;

    double inRadius = 3*volume/area;

    //  May be negative due to inexact arithmetic
    return std::abs(circumRadius / inRadius);

}

/** \brief Improve maximal aspect ratio of the grid by moving vertices.
 *
 *  \param grid The grid
 *  \param threshold The threshold on the aspect ratio that determines which elements should be refined.
 */
template <class GridType>
void improveGrid(GridType& grid, double threshold)
{
    const int dim = GridType::dimension;
    int maxLevel = grid.maxLevel();

    if (maxLevel==0)
        return;

    // Only works in 3d
    if (dim!=3)
        DUNE_THROW(Dune::GridError, "Grid improvement only for 3d grids!");

    const auto& maxLevelView = grid.levelGridView(maxLevel);
    const auto& indexSet = maxLevelView.indexSet();

    Dune::BitSetVector<1> isNewOnThisLevel(indexSet.size(dim), false);
    std::vector<Dune::FieldVector<double, dim> > vertexPosition(indexSet.size(dim));
    std::vector<Dune::FieldVector<double, dim> > bisectionPosition(indexSet.size(dim));

    // Copy all vertex positions into an indexable data structure
    for (const auto& v : vertices(maxLevelView))
        vertexPosition[indexSet.index(v)] = v.geometry().corner(0);

    // ///////////////////////////////////////////////////////////////
    //   Create the whole grid boundary as a boundary patch
    // ///////////////////////////////////////////////////////////////

    BoundaryPatch<typename GridType::LevelGridView> boundary(maxLevelView, true);

    std::cout << "Boundary contains " << boundary.numVertices() << " vertices\n";

    // /////////////////////////////////////////////////////////////
    //   We need to know which vertices is new on the maxlevel and which
    //   vertices are descendants of vertices on coarser levels.
    // /////////////////////////////////////////////////////////////

    double edgeMidPoints[6][3] = {{0.5,0,0},
                                  {0.5,0.5,0},
                                  {0,0.5,0},
                                  {0,0,0.5},
                                  {0.5,0,0.5},
                                  {0,0.5,0.5}};

    for (const auto& e : elements(maxLevelView)) {

        const auto& father = e.father();

        const auto geometryInFather = e.geometryInFather();
        for (size_t i=0; i<e.subEntities(dim); i++) {

            const auto& positionInFather = geometryInFather.corner(i);

            for (int j=0; j<6; j++) {
                if ( std::abs(positionInFather[0]-edgeMidPoints[j][0]) < 0.1
                     && std::abs(positionInFather[1]-edgeMidPoints[j][1]) < 0.1
                     && std::abs(positionInFather[2]-edgeMidPoints[j][2]) < 0.1) {

                    isNewOnThisLevel[indexSet.subIndex(e,i,dim)][0] = true;
                    bisectionPosition[indexSet.subIndex(e,i,dim)] = father.geometry().global(positionInFather);
                    break;
                }
            }
        }
    }

    // ///////////////////////////////////////////////////////////////
    //   Mark the vertices of all 'bad' elements
    // ///////////////////////////////////////////////////////////////
    Dune::BitSetVector<1> badVertices(indexSet.size(dim),false);
    for (const auto& e : elements(maxLevelView)){

        Dune::FieldVector<double,dim> dummy(0.5);

        double indicator = aspectRatio(e);
        if (indicator > threshold || e.geometry().jacobianInverseTransposed(dummy).determinant() < 0) {

            // This element is of bad quality.  Mark all its vertices
            for (size_t i=0; i<e.subEntities(dim); i++){
                if (boundary.containsVertex(indexSet.subIndex(e,i,dim))) {
                    badVertices[indexSet.subIndex(e,i,dim)][0] = true;
                }
            }
        }
    }

    std::cout << "The grid has " << badVertices.count() << " bad vertices!" << std::endl;

    // Loop over all new vertices
    for (const auto& v : vertices(maxLevelView)) {

        // Get the index
        int vIdx = indexSet.index(v);

        if (!isNewOnThisLevel[vIdx][0] || !badVertices[vIdx][0])
            continue;

        grid.setPosition(v, bisectionPosition[vIdx]);
    }
}


template <class GridView>
void printAspectRatios(const GridView& gridView)
{
    double max(0);
    double min(std::numeric_limits<double>::max());

    for (const auto& e : elements(gridView)) {

        double indicator = aspectRatio(e);

        max = std::max(max ,indicator);
        min = std::min(min ,indicator);
    }
    std::cout<<"Aspect ratios range from "<<min<<" -- "<<max<<std::endl;
}

#endif
