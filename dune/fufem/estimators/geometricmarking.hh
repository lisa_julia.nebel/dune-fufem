#ifndef DUNE_GEOMETRIC_ERROR_ESTIMATOR_HH
#define DUNE_GEOMETRIC_ERROR_ESTIMATOR_HH

#include <dune/common/fvector.hh>

/** \brief Error estimator which marks an element for refinement if it fulfills a geometric condition
 */
template <class GridType>
class GeometricEstimator {

    enum {dim      = GridType::dimension};
    enum {dimworld = GridType::dimensionworld};

 public:
    static void estimate(GridType& grid,
                  bool (*refinementCondition)(const Dune::FieldVector<double, dim>& pos)) {

        for (const auto& e : elements(grid.leafGridView())) {

            // Compute element's center of mass
            const auto& geometry = e.geometry();
            auto center = geometry.corner(0);

            for (int i=1; i<geometry.corners(); i++)
                center += geometry.corner(i);

            center /= (double)geometry.corners();

            // Mark if the condition is fulfilled
            if ((*refinementCondition)(center) )
                grid.mark(1, e);
        }
    }

    template <class BlockVectorType>
    static void estimate(GridType& grid,
                  bool (*refinementCondition)(const Dune::FieldVector<double, dimworld>& pos),
                  const BlockVectorType& deformation) {

        const auto& indexSet = grid.leafIndexSet();

        for (const auto& e : elements(grid.leafGridView())) {

            // Compute element's center of mass
            Dune::FieldVector<double, dimworld> center(0);
            const auto& geometry = e.geometry();

            for (int i=0; i<geometry.corners(); i++)
                center += geometry.corner(i) + deformation[indexSet.template subIndex<dim>(e,i)];

            center /= (double) geometry.corners();

            // Mark if the condition is fulfilled
            if ( (*refinementCondition)(center) > 0)
                grid.mark(1, e);
        }
    }
};

#endif
