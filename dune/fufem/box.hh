#ifndef BOX_HH
#define BOX_HH

/** \brief Simple Box class
  */
template<typename CoordType, int dim>
class Box
{
public:

    ~Box()
    {}

    Box(const CoordType& lower, const CoordType& upper) : lower_(lower), upper_(upper)
    {
        for (int i = 0; i < dim; ++i)
            center_[i] = 0.5*(upper_[i]+lower_[i]);
    }

    Box(const Box& b) : lower_(b.lower_), upper_(b.upper_)
    {
        for (int i = 0; i < dim; ++i)
            center_[i] = 0.5*(upper_[i]+lower_[i]);
    }

    bool contains(const CoordType& c) const
    {
        for (int i = 0; i < dim; ++i)
            if (c[i] < this->lower_[i] || c[i] >= this->upper_[i])
                return false;
        return true;
    }

    bool intersects(const Box& b)
    {
        for (int i = 0; i < dim; ++i)
            if (this->lower_[i] >= b.upper_[i] || b.lower_[i] >= this->upper_[i])
                return false;
        return true;
    }

    const CoordType& center() const
    {
        return this->center_;
    }


    double size(int i)
    {
        return upper_[i]-lower_[i];
    }

    const CoordType& lower() const
    {
        return lower_;
    }

    const CoordType& upper() const
    {
        return upper_;
    }

protected:
    CoordType  lower_;
    CoordType  upper_;
    CoordType  center_;
};


#endif
