#include <config.h>

#include <iostream>

#include <dune/fufem/symmetricmatrix.hh>
#include <dune/common/test/testsuite.hh>

using namespace Dune;


template <int N>
TestSuite testIsometry()
{
    TestSuite t;

    FieldVector<double,N*(N+1)/2> v;
    for (std::size_t i=0; i<v.size(); i++)
        v[i] = 1.0 + i*i - 4*i; // random values

    // compute Frobenius norm explicitly,
    // since frobenius_norm() returns v.two_norm() :)
    Fufem::SymmetricMatrix<double,N> M(v.begin());
    double result = 0;
        for (size_t i=0; i<N; i++)
        for (size_t j=0; j<=i; j++)
            result += ((i==j) ? 1.0 : 2.0) * M(i,j) * M(i,j);

    result = std::sqrt(result);

    t.check( std::abs(result - v.two_norm()) < 1e-10)
            << "Mapping v -> B(v) is not an isometry";

    return t;
}


template <int N>
TestSuite testEnergyScalarProduct()
{
    TestSuite t;

    FieldVector<double,N*(N+1)/2> v;
    for (std::size_t i=0; i<v.size(); i++)
        v[i] = 4*i + 2; // random values

    // create matrix
    Fufem::SymmetricMatrix<double,N> M(v.begin());

    // some random left/right vectors
    FieldVector<double,N> v1,v2;
    for(int i=0; i<N; i++)
    {
        v1[i] = i*i -5*i;
        v2[i] = i + 3;
    }

    // compute v1^T*M*V2 by hand:
    double result = 0;
    for( int i=0; i<N; i++)
        for( int j=0; j<N; j++)
            result += v1[i] * M(i,j) * v2[j];

    // diff to built-in function
    result -= M.energyScalarProduct(v1,v2);

    t.check( std::abs(result) < 1e-10)
            << "energyScalarProduct returns wrong value!";

    return t;
}

int main()
{
    TestSuite t;

    t.subTest(testIsometry<1>());
    t.subTest(testIsometry<2>());
    t.subTest(testIsometry<3>());
    t.subTest(testIsometry<4>());
    t.subTest(testIsometry<5>());
    t.subTest(testIsometry<6>());
    t.subTest(testIsometry<7>());
    t.subTest(testIsometry<8>());

    t.subTest(testEnergyScalarProduct<1>());
    t.subTest(testEnergyScalarProduct<2>());
    t.subTest(testEnergyScalarProduct<3>());
    t.subTest(testEnergyScalarProduct<4>());
    t.subTest(testEnergyScalarProduct<5>());
    t.subTest(testEnergyScalarProduct<6>());
    t.subTest(testEnergyScalarProduct<7>());
    t.subTest(testEnergyScalarProduct<8>());

    return t.exit();
}
