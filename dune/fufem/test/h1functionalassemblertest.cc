#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/version.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/h1functionalassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

#include "common.hh"


/** \brief TestSuite for H1FunctionalAssembler
 *
 *  This TestSuite tests for consistency of the H1FunctionalAssembler with the LaplaceAssembler, i.e. it wraps canonical
 *  P1BasisFunctions such that the wrapper returns the Derivative and plugs this wrapper into the H1FunctionalAssembler.
 *  Values should then correspond to those in the discrete Laplacian.
 */
struct H1FunctionalAssemblerTestSuite
{
    template <typename GridType>
    bool check(const GridType& grid)
    {
        using Basis = Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1>;
        using FufemBasis = DuneFunctionsBasis<Basis>;
        using FE = typename FufemBasis::LocalFiniteElement;

        const int block_size=2;

        Basis basis(grid.leafGridView());
        FufemBasis fufemBasis(basis);

        /* the canonical basis functions and its derivative */
        typedef typename Dune::BlockVector<Dune::FieldVector<double,1> > ScalarCoefficients;
        typedef typename Dune::BlockVector<Dune::FieldVector<double,block_size> > VectorCoefficients;

        ScalarCoefficients coeffs(basis.size());
        VectorCoefficients coeffsVec(basis.size());
        coeffs = 0.0;
        coeffsVec = 0.0;

        auto scalar_basis_function = Dune::Functions::makeDiscreteGlobalBasisFunction<ScalarCoefficients::value_type>(basis, coeffs);
        auto vector_basis_function = Dune::Functions::makeDiscreteGlobalBasisFunction<VectorCoefficients::value_type>(basis, coeffsVec);

        auto scalar_basis_function_derivative = derivative(scalar_basis_function);
        auto vector_basis_function_derivative = derivative(vector_basis_function);


        /* containers for stiffness matrix and assembled functional */
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat;
        Dune::BlockVector<Dune::FieldVector<double,1> > f_i(basis.size());
        Dune::BlockVector<Dune::FieldVector<double,block_size> > g_i(basis.size());

        /* create assemblers and assemble Laplacian */
        Dune::Fufem::DuneFunctionsOperatorAssembler<Basis,Basis> operatorAssembler(basis, basis);
        Dune::Fufem::LaplaceAssembler laplaceAssembler;
        operatorAssembler.assembleBulk(Dune::Fufem::istlMatrixBackend(stiff_mat), laplaceAssembler);

        Assembler<FufemBasis, FufemBasis> assembler(fufemBasis, fufemBasis);

        auto quadKey = QuadratureRuleKey(GridType::dimension,2);

        H1FunctionalAssembler<GridType,FE> h1functionalassembler(scalar_basis_function_derivative, quadKey);
        H1FunctionalAssembler<GridType, FE,Dune::FieldVector<double,block_size> > h1functionalassemblerVec(vector_basis_function_derivative, quadKey);

//        for (size_t i=0; i<basis.size(); ++i)
        for (size_t i=0; i<basis.size(); i+=20)
        {
            /* set canonical basis function */
            coeffs = 0.0;
            coeffs[i] = 1.0;
            coeffsVec = 0.0;
            coeffsVec[i] = 1.0;

            /* assemble corresponding h1 functional */
            assembler.assembleFunctional(h1functionalassembler, f_i);
            assembler.assembleFunctional(h1functionalassemblerVec, g_i);

            for (size_t j=0; j<basis.size(); ++j)
            {
                if ((stiff_mat.exists(i,j) && std::abs(stiff_mat[i][j]-f_i[j])>1e-14) or (not(stiff_mat.exists(i,j)) && std::abs(f_i[j])>1e-14))
                {
                    std::cout << "stiff_mat(" << i << ", " << j << ")= ";
                    if (stiff_mat.exists(i,j))
                        std::cout << stiff_mat[i][j];
                    else
                        std::cout <<  0.0;
                    std::cout << "  f_" << i << "(" << j << ")= " << f_i[j] << std::endl;
                    return false;
                }
                for (int k=0; k<block_size; ++k)
                {
                    if ((stiff_mat.exists(i,j) && std::abs(stiff_mat[i][j]-g_i[j][k])>1e-14) or (not(stiff_mat.exists(i,j)) && std::abs(g_i[j][k])>1e-14))
                    {
                        std::cout << "stiff_mat(" << i << ", " << j << ")= ";
                        if (stiff_mat.exists(i,j))
                            std::cout << stiff_mat[i][j];
                        else
                            std::cout <<  0.0;
                        std::cout << "  g_" << i << "(" << j << ")[" << k << "]= " << g_i[j][k] <<  "  f_" << i << "(" << j << ")= " << f_i[j] << std::endl;
                        std::cout << std::endl << "H1FunctionalAssemblerTest failed" << std::endl << std::endl;
                        return false;
                    }
                }
            }

        }

        return true;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the H1FunctionalAssemblerTest" << std::endl;

    bool passed = true;

// Only test with dune-functions from version 2.9, because
// derivative(DiscreteGlobalBasisFunction) is not implemented
// in older versions.
#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 9)
    H1FunctionalAssemblerTestSuite tests;
    passed = checkWithStandardAdaptiveGrids(tests);
#else
    #warning h1functionalassemblertest.cc does not test anything for dune-functions before version 2.9.
#endif

    return passed ? 0 : 1;

}
