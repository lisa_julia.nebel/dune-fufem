#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/grid/common/gridfactory.hh>
#include <dune/alugrid/grid.hh>
#endif

#include <dune/fufem/test/common.hh>
#include <dune/fufem/geometry/refinedsimplexgeometry.hh>

class MakeRefinedSimplexGeometryTestSuite
{
    public:
    typedef typename RefinedSimplexGeometry<double,2,3>::GlobalCoordinate WorldCoords;
    typedef typename RefinedSimplexGeometry<double,2,3>::LocalCoordinate LocalCoords;

    MakeRefinedSimplexGeometryTestSuite()
    {}


    Dune::TestSuite test()
    {
      Dune::TestSuite test;
#if HAVE_DUNE_ALUGRID
        typedef Dune::ALUGrid<2,3,Dune::simplex, Dune::conforming> GridType;

        Dune::GridFactory<GridType> factory;

        std::vector<WorldCoords> vertices(3);
        std::vector<std::vector<unsigned int> > faces(1);

        double sqrt_two = std::sqrt(2.0);
        double sqrt_six = std::sqrt(6.0);

        vertices = {{-sqrt_two/4, -sqrt_six/4, sqrt_two/2}, {sqrt_two/2, 0, sqrt_two/2}, {-sqrt_two/4, sqrt_six/4, sqrt_two/2}};
        faces = {{0,1,2}};

        for (auto vertex : vertices)
        {
            factory.insertVertex(vertex);
        }

        for (auto face : faces)
        {
            factory.insertElement(Dune::GeometryType(Dune::GeometryType::simplex,2),face);
        }

        auto grid = factory.createGrid();

        auto eltIt = grid->leafGridView().begin<0>();
        auto transformation = [] (const WorldCoords& x) {
          WorldCoords y = x;
          y /= x.two_norm();
          return y;
        };


        RefinedSimplexGeometry<typename GridType::ctype, 2,3> refinedGeometry = makeRefinedSimplexGeometry(*eltIt,transformation);

        {
            auto xLocal = LocalCoords{0.0, 0.5};
            auto xGlobal = eltIt->geometry().global(xLocal);
            auto xMappedGlobal = transformation(xGlobal);
            test.check(isCloseDune(xMappedGlobal, refinedGeometry.global(xLocal)))
              << "Edge center (" << xLocal << ") has wrong global coordinate. Should be (" << xMappedGlobal << ") but is (" << refinedGeometry.global(xLocal) << ").";
        }
        {
            auto xLocal = LocalCoords{0.5, 0.5};
            auto xGlobal = eltIt->geometry().global(xLocal);
            auto xMappedGlobal = transformation(xGlobal);
            test.check(isCloseDune(xMappedGlobal, refinedGeometry.global(xLocal)))
              << "Edge center (" << xLocal << ") has wrong global coordinate. Should be (" << xMappedGlobal << ") but is (" << refinedGeometry.global(xLocal) << ").";
        }
        {
            auto xLocal = LocalCoords{0.5, 0.0};
            auto xGlobal = eltIt->geometry().global(xLocal);
            auto xMappedGlobal = transformation(xGlobal);
            test.check(isCloseDune(xMappedGlobal, refinedGeometry.global(xLocal)))
              << "Edge center (" << xLocal << ") has wrong global coordinate. Should be (" << xMappedGlobal << ") but is (" << refinedGeometry.global(xLocal) << ").";
        }
        {
            auto xLocal = LocalCoords{1.0/3.0, 1.0/3.0};
            // Center of element should be the center of refined sub-element
            // spanned by the transformed edge midpoints.
            auto xMappedGlobal = WorldCoords{0.0,0.0,0.0};
            xMappedGlobal += transformation(eltIt->geometry().global(LocalCoords{0.0, 0.5}));
            xMappedGlobal += transformation(eltIt->geometry().global(LocalCoords{0.5, 0.5}));
            xMappedGlobal += transformation(eltIt->geometry().global(LocalCoords{0.5, 0.0}));
            xMappedGlobal /= 3.0;
            test.check(isCloseDune(xMappedGlobal, refinedGeometry.global(xLocal)))
              << "Element center (" << xLocal << ") has wrong global coordinate. Should be (" << xMappedGlobal << ") but is (" << refinedGeometry.global(xLocal) << ").";
        }
#endif
        return test;
    }

};

int main (int argc, char *argv[])
{
      Dune::MPIHelper::instance(argc, argv);
      MakeRefinedSimplexGeometryTestSuite testSuite;
#if HAVE_DUNE_ALUGRID
      return testSuite.test().exit();
#else
      return 77;
#endif
}
