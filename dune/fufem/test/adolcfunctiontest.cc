#include <config.h>

#include <cassert>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/version.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <dune/localfunctions/lagrange/lagrangesimplex.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/fufem/functions/adolcfunction.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>

#include <dune/common/test/testsuite.hh>

template<class Signature>
struct DerivativeTraits : public Dune::Functions::DefaultDerivativeTraits<Signature>
{};

template<class K>
struct DerivativeTraits<K(std::vector<K>)>
{
  using Range = std::vector<K>;
};

template<class K>
struct DerivativeTraits<std::vector<K>(std::vector<K>)>
{
  using Range = Dune::Matrix<K>;
};



template<class Geometry>
auto quadratureRule(const Geometry& geometry, unsigned int order)
{
  constexpr auto dim = Geometry::mydimension;
  const auto& rule = Dune::QuadratureRules<double, dim>::rule(geometry.type(), order);
  return Dune::transformedRangeView(rule, [&](auto&& qp) {
    return std::pair(qp.position(), qp.weight());
  });
}



template<class LocalView, class LocalCoeff>
auto localDirichletEnergy(const LocalView& localView, const LocalCoeff& localCoeff)
{
  constexpr auto dim = LocalView::Element::Geometry::mydimension;
  using LocalJacobian = typename LocalView::Tree::FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
  using K = typename LocalCoeff::value_type;
  using Jacobian = Dune::FieldVector<K, dim>;

  K result = 0;
  auto localBasisJacobians = std::vector<LocalJacobian>{};
  auto&& geometry = localView.element().geometry();
  auto&& localBasis = localView.tree().finiteElement().localBasis();
  auto order = 2*localBasis.order();
  for(auto [position, weight] : quadratureRule(geometry, order))
  {
    auto jacobian = Jacobian();
    jacobian = 0;
    localBasis.evaluateJacobian(position, localBasisJacobians);
    auto integrationElement = geometry.integrationElement(position);
    auto elementJacobianInverse = geometry.jacobianInverse(position);
    for (auto i : Dune::range(localView.size()))
    {
      Jacobian globalBasisJacobian = (localBasisJacobians[i] * elementJacobianInverse)[0];
      globalBasisJacobian *= localCoeff[i];
      jacobian += globalBasisJacobian;
    }
    result += 0.5 * jacobian.two_norm2() * integrationElement * weight;
  }
  return result;
}



// Generate a local operator assembler assembling the Hessian of the local energy functional
template<class LocalEnergy, class GlobalCoefficients>
auto localHessianAssembler(const LocalEnergy& localEnergy, const GlobalCoefficients& globalCoefficients)
{

  // This is mostly boiler plate code to
  // a) adapt the local energy to the dune-functions interface
  // b) wrap the Hessian computation to the local assembler interface
  return [&](const auto& element, auto& localMatrix, const auto& trialLocalView, const auto& ansatzLocalView)
  {
    using Domain = std::vector<double>;
    using Range = double;
    using SignatureTag = Dune::Functions::SignatureTag<Range(Domain), DerivativeTraits>;

    auto f = Dune::Fufem::makeAdolCFunction(SignatureTag(), [&](const auto& x) { return localEnergy(trialLocalView, x); });
    auto ddf = derivative(derivative(f));

    auto localCoefficients = Domain();
    localCoefficients.resize(trialLocalView.size());
    for(auto i : Dune::range(trialLocalView.size()))
      localCoefficients[i] = globalCoefficients[trialLocalView.index(i)];

    auto localHessian = ddf(localCoefficients);

    for(auto row : Dune::range(localHessian.N()))
      for(auto col : Dune::range(localHessian.M()))
        localMatrix[row][col] += localHessian[row][col];
  };

}



int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

  auto suite = Dune::TestSuite();

  {
    using Domain = Dune::FieldVector<double,3>;
    using Range = double;
    using SignatureTag = Dune::Functions::SignatureTag<Range(Domain)>;
    using DerivativeRange = SignatureTag::DerivativeTraits<SignatureTag::Signature>::Range;
    using SecondDerivativeRange = SignatureTag::DerivativeTraits<DerivativeRange(Domain)>::Range;

    auto f_raw = [](auto x) {
      return x[0]*x[0] + x[1]*x[2] + sin(2*M_PI*x[0])/(2*M_PI);
    };

    auto df_raw = [](auto& x) {
      return DerivativeRange({2*x[0] + cos(2*M_PI*x[0]), x[2], x[1] });
    };

    auto ddf_raw = [](auto& x) {
      SecondDerivativeRange H;
      H[0] = {2+sin(2*M_PI*x[0])*2*M_PI, 0, 0};
      H[1] = {0, 0, 1};
      H[2] = {0, 1, 0};
      return H;
    };

    auto f = Dune::Fufem::makeAdolCFunction(SignatureTag(), f_raw);
    auto df = derivative(f);
    auto ddf = derivative(df);

    auto x = Domain({1,2,3});

    suite.check(f(x)==f_raw(x))
      << "AdolCFunction differs from original function";
    suite.check((df(x)-df_raw(x)).infinity_norm() < 1e-14)
      << "First derivative of AdolCFunction differs from manually implemented derivative by " << (df(x)-df_raw(x)).infinity_norm();
    suite.check((ddf(x)-ddf_raw(x)).infinity_norm() < 1e-14)
      << "Second derivative of AdolCFunction differs from manually implemented derivative by " << (ddf(x)-ddf_raw(x)).infinity_norm();
  }

#if DUNE_VERSION_GTE(DUNE_GEOMETRY, 2, 9)
  {
    // Create a basis for testing
    using namespace Dune::Functions::BasisFactory;
    auto gridPtr = Dune::StructuredGridFactory<Dune::YaspGrid<2>>::createCubeGrid({0,0}, {1,1}, {10,10});
    auto gridView = gridPtr->leafGridView();
    auto basis = makeBasis(gridView, lagrange<2>());

    // Create some coefficient vector for testing
    auto coefficients = std::vector<double>{};
    auto f = [](const auto& x) {
      return x[0]*x[0] + x[1]*x[1];
    };
    Dune::Functions::interpolate(basis, coefficients, f);

    // To avoid having to specify template parameters we make the
    // Dirichlet energy a function object.
    auto localEnergy = [&](const auto& localView, const auto& x) {
      return localDirichletEnergy(localView, x);
    };

    // Create local Hessian assembler for local energy.
    // This will compute the Hessian using automated differentiation.
    auto hessianAssembler = localHessianAssembler(localEnergy, coefficients);

    // For comparison create a manual Laplace assembler.
    auto laplaceAssembler = Dune::Fufem::LaplaceAssembler();

    auto localView = basis.localView();
    auto localMatrix = Dune::Matrix<double>();
    for(const auto& e : Dune::elements(basis.gridView()))
    {
      localView.bind(e);
      localMatrix.setSize(localView.size(), localView.size());
      localMatrix = 0;

      // Assemble local matrix using AD-driven Hessian assembler
      hessianAssembler(localView.element(), localMatrix, localView, localView);

      // For comparison assemble the same matrix using Laplace assembler.
      auto referenceLocalMatrix = localMatrix;
      referenceLocalMatrix = 0;
      laplaceAssembler(localView.element(), referenceLocalMatrix, localView, localView);

      // Compare local matrices using a relative tolerance.
      auto tol = 1.0e-14 * referenceLocalMatrix.infinity_norm();
      localMatrix -= referenceLocalMatrix;

      suite.check(localMatrix.infinity_norm() < tol)
        << "Local matrix assembled using AD differs from LaplaceAssembler by " << localMatrix.infinity_norm();
        ;
    }
  }
#endif

  return suite.exit();
}
