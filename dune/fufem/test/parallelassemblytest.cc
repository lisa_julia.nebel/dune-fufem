// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <vector>
#include <cmath>

#include <dune/common/version.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/uggrid.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/boundarydofs.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/fufem/parallel/elementcoloring.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/dunefunctionsfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>



template<int dim>
auto createUniformCubeGrid()
{
  using Grid = Dune::YaspGrid<dim>;
  Dune::FieldVector<double,dim> l(1.0);
  std::array<int,dim> elements;
  for(auto& e : elements)
    e=2;
  return std::make_unique<Grid>(l, elements);
}



#if HAVE_DUNE_UGGRID
auto createDisconnectedMixedGrid()
{
  using Grid = Dune::UGGrid<2>;
  Dune::GridFactory<Grid> factory;
  for(unsigned int k : Dune::range(9))
    factory.insertVertex({0.5*(k%3), 0.5*(k/3)});
  for(unsigned int k : Dune::range(4))
    factory.insertVertex({2.0+0.5*(k%2), 0.5*(k/2)});
  factory.insertElement(Dune::GeometryTypes::cube(2), {0, 1, 3, 4});
  factory.insertElement(Dune::GeometryTypes::cube(2), {1, 2, 4, 5});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {3, 4, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 7, 6});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 5, 7});
  factory.insertElement(Dune::GeometryTypes::simplex(2), {5, 8, 7});
  factory.insertElement(Dune::GeometryTypes::cube(2), {9, 10, 11, 12});
  return std::unique_ptr<Grid>{factory.createGrid()};
}
#endif



// Create a local functional assembler for assembling L^2 functionals
// with given function. Notice that this does not support grid functions.
template<class F>
auto localFunctionalAssembler(F f, QuadratureRuleKey functionQuadKey = QuadratureRuleKey())
{
  return [=](const auto& element, auto& localVector, const auto& localView)
  {
    using LocalView = std::decay_t<decltype(localView)>;
    using Range = typename LocalView::Tree::FiniteElement::Traits::LocalBasisType::Traits::RangeType;
    constexpr auto dim = LocalView::Element::Geometry::mydimension;

    auto&& geometry = localView.element().geometry();
    auto&& finiteElement = localView.tree().finiteElement();
    auto basisValues = std::vector<Range>();
    
    auto quadKey = QuadratureRuleKey(finiteElement).product(functionQuadKey);
    const auto& rule = QuadratureRuleCache<double, dim>::rule(quadKey);

    for(auto i : Dune::range(rule.size()))
    {
      auto integrationElement = geometry.integrationElement(rule[i].position());
      auto y = f(geometry.global(rule[i].position()));
      finiteElement.localBasis().evaluateFunction(rule[i].position(), basisValues);
      for (auto i : Dune::range(localView.size()))
        localVector[i] += basisValues[i] * y * integrationElement;
    }
  };
}



template<class Basis>
auto testParallelAssembly(const Basis& basis, std::size_t threadCount)
{
  Dune::TestSuite suite;

  std::cout << "****************************************" << std::endl;
  std::cout << "Testing with " << Dune::className<typename Basis::GridView::Grid>() << std::endl;
  std::cout << "Number of elements is " << basis.gridView().size(0) << std::endl;
  std::cout << "Number of DOFs is " << basis.dimension() << std::endl;

  // Create stiffness matrix, rhs vector, and rhs function

  using Vector = Dune::BlockVector<double>;
  using Matrix = Dune::BCRSMatrix<double>;

  auto rhsVector = Vector();
  auto stiffnessMatrix = Matrix();

  auto rhsFunction = [] (const auto& x) { return 10;};

  // Compute a colored partition of the grid view
  auto gridViewPartition = Dune::Fufem::coloredGridViewPartition(basis.gridView(), true);

  for(auto&& elementRange : gridViewPartition)
    suite.check(Dune::Fufem::checkOverlapFree(elementRange, basis));

  // Assemble stiffness matrix
  {
    auto timer = Dune::Timer();
    auto operatorAssembler = Dune::Fufem::DuneFunctionsOperatorAssembler{basis, basis};
    auto matrixBackend = Dune::Fufem::istlMatrixBackend(stiffnessMatrix);
    auto localAssembler = Dune::Fufem::LaplaceAssembler();

    timer.reset();
    operatorAssembler.assembleBulk(matrixBackend, localAssembler, gridViewPartition, threadCount);
    std::cout << "Assembling the matrix with " << threadCount << " threads took " << timer.elapsed() << "s" << std::endl;

    // Just for fun: Compute a coloring of the assembled stiffness matrix
    auto matrixAdjacency = [&](auto row) {
      return Dune::iteratorTransformedRangeView(stiffnessMatrix[row], [](const auto& it) { return it.index(); });
    };
    timer.reset();
    auto matrixColoring = Dune::Fufem::computeColoring(matrixAdjacency, stiffnessMatrix.N());
    std::cout << "Computing a coloring of the of matrix took " << timer.elapsed() << "s" << std::endl;

    std::vector<std::size_t> colorSizes;
    for(auto color : matrixColoring)
    {
      if (colorSizes.size() < color+1)
        colorSizes.resize(color+1, 0);
      ++colorSizes[color];
    }
    std::cout << "Colors used in matrix coloring : " << colorSizes.size() << std::endl;
    std::cout << "Size of color blocks of matrix :";
    for(auto colorSize : colorSizes)
      std::cout << " " << colorSize;
    std::cout << std::endl;
  }

  // Assemble rhs vector
  {
    auto timer = Dune::Timer();
    auto functionalAssembler = Dune::Fufem::DuneFunctionsFunctionalAssembler{basis};
    auto rhsBackend = Dune::Functions::istlVectorBackend(rhsVector);
    auto localAssembler = localFunctionalAssembler(rhsFunction);

    timer.reset();
    functionalAssembler.assembleBulk(rhsBackend, localAssembler, gridViewPartition, threadCount);
    std::cout << "Assembling the rhs with " << threadCount << " threads took " << timer.elapsed() << "s" << std::endl;
  }

  return suite;
}




int main (int argc, char *argv[]) try
{
  // Set up MPI, if available
  Dune::MPIHelper::instance(argc, argv);

  Dune::TestSuite suite;

  // There are no viewThreadSafe grids in dune 2.8
  // so we have to skip the whole test.
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 8)

  // Allow to pass number of threads as command line argument. Default is 4.
  std::size_t threadCount = 4;
  if (argc>1)
    threadCount = std::stoul(std::string(argv[1]));

  using namespace Dune::Functions::BasisFactory;

  {
    auto gridPtr = createUniformCubeGrid<2>();
    gridPtr->globalRefine(4);
    auto basis = makeBasis(gridPtr->leafGridView(), lagrange<3>());
    suite.subTest(testParallelAssembly(basis, threadCount));
  }

  {
    auto gridPtr = createUniformCubeGrid<3>();
    gridPtr->globalRefine(2);
    auto basis = makeBasis(gridPtr->leafGridView(), lagrange<2>());
    suite.subTest(testParallelAssembly(basis, threadCount));
  }

// UGGrid is not viewThreadSafe before 2.10
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
#if HAVE_DUNE_UGGRID
  {
    auto gridPtr = createDisconnectedMixedGrid();
    gridPtr->globalRefine(4);
    auto basis = makeBasis(gridPtr->leafGridView(), lagrange<3>());
    suite.subTest(testParallelAssembly(basis, threadCount));
  }

  {
    auto gridPtr = Dune::StructuredGridFactory<Dune::UGGrid<3>>::createSimplexGrid({{0,0,0}}, {{1,1,1}}, {{2,2,2}});
    gridPtr->globalRefine(2);
    auto basis = makeBasis(gridPtr->leafGridView(), lagrange<1>());
    suite.subTest(testParallelAssembly(basis, threadCount));
  }
#endif // HAVE_DUNE_UGGRID
#endif // DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)

#else
    #warning parallelassemblytest.cc does not test anything with dune-functions 2.8, because there is no viewThreadSafe grid.
#endif //DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 8)
  return suite.exit();
}
catch (std::exception& e) {
  std::cout << e.what() << std::endl;
}
