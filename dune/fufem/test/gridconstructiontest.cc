#include <config.h>
#include <iostream>

#include <dune/common/parametertree.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/fufem/utilities/gridconstruction.hh>

#include <dune/grid/onedgrid.hh>

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
template <size_t dim>
using SN_ALUGrid = Dune::ALUGrid<dim, dim, Dune::ALUGridElementType::simplex,
                                 Dune::ALUGridRefinementType::nonconforming>;
#endif // HAVE_DUNE_ALUGRID

struct GridConstructionTest {

  template <class Grid>
  bool test1D() const {
    using C = Dune::GridConstruction<Grid, 1>;
    return test([this]() { C::createGrid(getIntervalConfig()); });
  }

  template <class Grid>
  bool test2D() const {
    using C = Dune::GridConstruction<Grid, 2>;
    return test([this]() {
      C::createGrid(getRectangleConfig());
      C::createGrid(getCircleConfig());
    });
  }

  template <class Grid>
  bool test3D() const {
    using C = Dune::GridConstruction<Grid, 3>;
    return test([this]() {
      C::createGrid(getTubeSegmentConfig());
      C::createGrid(getSphereConfig());
      C::createGrid(getCuboidConfig());
    });
  }

private:
  template <class T>
  bool test(T&& t) const {
    try {
      t();
    } catch (const Dune::Exception& e) {
      std::cout << "Exception caught:" << e << std::endl;
      return false;
    } catch (...) {
      std::cout << "Unknown exception caught." << std::endl;
      return false;
    }
    return true; // no exception was caught
  }

  using Config = Dune::ParameterTree;

  Config getIntervalConfig() const {
    Config config;
    config["type"] = "interval";
    config["lowerCorner"] = "0.0";
    config["upperCorner"] = "1.0";
    config["elements"] = "3";
    return config;
  }

  Config getRectangleConfig() const {
    Config config;
    config["type"] = "rectangle";
    config["lowerCorner"] = "0.0 0.0";
    config["upperCorner"] = "1.0 1.0";
    config["elements"] = "3 3";
    config["tetrahedral"] = "1";
    return config;
  }

  Config getCircleConfig() const {
    Config config;
    config["type"] = "circle";
    config["center"] = "0.0 0.0";
    config["radius"] = "1.0";
    return config;
  }

  Config getTubeSegmentConfig() const {
    Config config;
    config["type"] = "tubeSegment";
    config["center"] = "0.0 0.0 0.0";
    config["thickness"] = "0.2";
    config["length"] = "2.0";
    config["innerRadius"] = "1.0";
    config["fromAngle"] = "0.0";
    config["toAngle"] = "10.0";
    config["nElementRing"] = "10";
    config["nElementLength"] = "5";
    config["closeTube"] = "0";
    config["axis"] = "0";
    config["tetrahedra"] = "1";
    config["parameterizedBoundary"] = "0";
    return config;
  }

  Config getSphereConfig() const {
    Config config;
    config["type"] = "sphere";
    config["center"] = "0.0 0.0 0.0";
    config["radius"] = "1.0";
    return config;
  }

  Config getCuboidConfig() const {
    Config config;
    config["type"] = "cuboid";
    config["lowerCorner"] = "0.0 0.0 0.0";
    config["upperCorner"] = "1.0 1.0 1.0";
    config["elements"] = "4 5 4";
    config["tetrahedral"] = "1";
    return config;
  }
};

int main(int argc, char** argv) {
  bool passed = true;
  Dune::MPIHelper::instance(argc, argv);

  std::cout << "This is GridConstructionTest v0.01" << std::endl;
  GridConstructionTest test;

  std::cout << "Testing OneD grid construction." << std::endl;
  passed = passed && test.test1D<Dune::OneDGrid>();

#if HAVE_DUNE_UGGRID
  std::cout << "Testing UG grid construction." << std::endl;
  passed = passed && test.test2D<Dune::UGGrid<2>>();
  passed = passed && test.test3D<Dune::UGGrid<3>>();
#else
  std::cout << "No UG Grid available." << std::endl;
#endif

#if HAVE_DUNE_ALUGRID
  std::cout << "Testing ALU grid construction." << std::endl;
  passed = passed && test.test2D<SN_ALUGrid<2>>();
  passed = passed && test.test3D<SN_ALUGrid<3>>();
#else // HAVE_DUNE_ALUGRID
  std::cout << "No ALU Grid available." << std::endl;
#endif // HAVE_DUNE_ALUGRID

  return passed ? 0 : 1;
}
