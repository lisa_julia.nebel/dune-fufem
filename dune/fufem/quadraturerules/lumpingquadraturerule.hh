#ifndef LUMPINGQUADRATURERULE_HH
#define LUMPINGQUADRATURERULE_HH

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/quadraturerules.hh>

/**
 * \brief A lumping quadrature rule
 *
 * This quadrature rule is supposed to be used for lumped
 * integrals. Its quadrature points are the vertices of
 * the reference element and all weights are equal
 * to the area of the element divided by the number
 * of vertices.
 *
 * \tparam ct The elementary type for coordinates
 * \tparam dim Dimension of integration domain
 *
 */
template<class ct, int dim>
class LumpingQuadratureRule :
    public Dune::QuadratureRule<ct, dim>
{
        typedef typename Dune::QuadratureRule<ct, dim> Base;
        typedef typename Dune::QuadraturePoint<ct, dim> QuadPoint;

    public:

        /**
         * \brief Construct lumping quadrature rule for given geometry
         */
        LumpingQuadratureRule(const Dune::GeometryType& gt) :
            Base(gt, 1)
        {
            auto refElement = Dune::ReferenceElements<ct, dim>::general(this->type());
            int size = refElement.size(dim);
            ct weight = refElement.volume()/size;
            this->reserve(size);
            for(int i=0; i<size; ++i)
                this->push_back(QuadPoint(refElement.position(i,dim), weight));
        }

};


#endif

