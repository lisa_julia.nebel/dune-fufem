#ifndef DUNE_FUFEM_FORMATSTRING_HH
#define DUNE_FUFEM_FORMATSTRING_HH

#warning This file is deprecated!  Please use Dune::formatString from the file dune/common/stringutility.hh!

#include <string>

#include <dune/common/stringutility.hh>

namespace Dune {
namespace Fufem {

/**
 * \brief Return formatted output for printf like string
 *
 * \param s Format string using printf syntax
 * \param args Arguments for format string
 * \returns Result of conversion as string
 */
template<class... T>
[[deprecated("Please use Dune::formatString() from dune/dommon/stringutility.hh a replacement.")]]
std::string formatString(const std::string& s, const T&... args)
{
  return Dune::formatString(s, args...);
}

}
}

#endif // DUNE_FUFEM_FORMATSTRING_HH
