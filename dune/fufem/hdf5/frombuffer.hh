#ifndef DUNE_FUFEM_HDF5_FROMBUFFER_HH
#define DUNE_FUFEM_HDF5_FROMBUFFER_HH

#include <numeric>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>

template <int k, typename ctype, typename T>
void fromBuffer(std::vector<ctype> const &buffer, std::array<T, 3> dimensions,
                Dune::Matrix<Dune::FieldVector<ctype, k>> &data) {
  auto const entrySize = std::accumulate(dimensions.begin(), dimensions.end(),
                                         1, std::multiplies<T>());
  assert(buffer.size() == entrySize);
  assert(dimensions[2] == k);
  data.setSize(dimensions[0], dimensions[1]);

  auto it = buffer.begin();
  for (auto &&row : data)
    for (auto &&localVector : row) {
      auto const nextIt = it + k;
      std::copy(it, nextIt, localVector.begin());
      it = nextIt;
    }
  assert(it == buffer.end());
}

template <int k, typename ctype, typename T>
void fromBuffer(std::vector<ctype> const &buffer, std::array<T, 2> &dimensions,
                Dune::BlockVector<Dune::FieldVector<ctype, k>> &data) {
  assert(buffer.size() == std::accumulate(dimensions.begin(), dimensions.end(),
                                          1ul, std::multiplies<T>()));
  assert(dimensions[1] == k);
  data.resize(dimensions[0]);

  auto it = buffer.begin();
  for (auto &&localVector : data) {
    auto const nextIt = it + k;
    std::copy(it, nextIt, localVector.begin());
    it = nextIt;
  }
  assert(it == buffer.end());
}

template <typename ctype, typename T>
void fromBuffer(std::vector<ctype> const &buffer, std::array<T, 1> dimensions,
                Dune::BlockVector<Dune::FieldVector<ctype, 1>> &data) {
  assert(buffer.size() == dimensions[0]);
  data.resize(dimensions[0]);

  auto it = buffer.begin();
  for (auto &&localVector : data)
    localVector[0] = *(it++);
}

template <typename ctype, typename T>
void fromBuffer(std::vector<ctype> const &buffer, std::array<T, 1> dimensions,
                std::vector<ctype> &data) {
  assert(buffer.size() == dimensions[0]);

  data = buffer;
}

template <typename ctype, typename T>
void fromBuffer(std::vector<ctype> const &buffer, std::array<T, 0> dimensions,
                ctype &data) {
  assert(buffer.size() == 1);
  data = buffer[0];
}
#endif
