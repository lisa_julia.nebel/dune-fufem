#ifndef DUNE_FUFEM_HDF5_TOBUFFER_HH
#define DUNE_FUFEM_HDF5_TOBUFFER_HH

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>

template <int k, typename ctype>
void toBuffer(Dune::Matrix<Dune::FieldVector<ctype, k>> const &data,
              std::vector<ctype> &buffer) {
  buffer.resize(data.N() * data.M() * k);
  auto it = buffer.begin();
  for (auto &&row : data)
    for (auto &&localVector : row)
      it = std::copy(localVector.begin(), localVector.end(), it);
}

template <int k, typename ctype>
void toBuffer(Dune::BlockVector<Dune::FieldVector<ctype, k>> const &data,
              std::vector<ctype> &buffer) {
  buffer.resize(data.size() * k);
  auto it = buffer.begin();
  for (auto &&localVector : data)
    it = std::copy(localVector.begin(), localVector.end(), it);
}

template <typename ctype>
void toBuffer(Dune::BlockVector<Dune::FieldVector<ctype, 1>> const &data,
              std::vector<ctype> &buffer) {
  buffer.resize(data.size());
  auto it = buffer.begin();
  for (auto &&localVector : data)
    *(it++) = localVector[0];
}

template <typename ctype>
void toBuffer(ctype const &data, std::vector<ctype> &buffer) {
  buffer.resize(1);
  buffer[0] = data;
}
#endif
