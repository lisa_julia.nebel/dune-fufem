// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_INDEXED_SLICE_ITERATOR_HH
#define DUNE_FUFEM_INDEXED_SLICE_ITERATOR_HH

#include <dune/common/iteratorfacades.hh>


/**
 * \brief An indexed iterator over a slice
 *
 * This class provides an iterator over a slice given by a base iterator
 * and an index() method. Both, the base iterator and the index are
 * incremented by a configurable stride value on incrementing the iterator.
 *
 * This is not a random access iterator. And indices are in general
 * not consecutive. Instead this can be considered as an iterator
 * over a sparse data set.
 *
 * \tparam BaseIterator Type of base iterator. Has to provide operator* and prefix operator++. May be a raw pointer.
 * \tparam Value Type of values the iterator iterates over.
 */
template<class BaseIterator, class Value>
class IndexedSliceIterator :
    public Dune::ForwardIteratorFacade<IndexedSliceIterator<BaseIterator, Value>,Value>
{
    typedef typename Dune::ForwardIteratorFacade<IndexedSliceIterator<BaseIterator, Value>,Value> Base;

public:
    typedef std::size_t size_type;

    IndexedSliceIterator() :
        index_(0),
        itStride_(1),
        indexStride_(1)
    {}

    /**
     * \brief Construct from value and index
     *
     * An end iterator is created by specifying an appropriate base iterator or index.
     * If itStride is zero to iterators are considered equal if the indices coincide.
     * Otherwise they are considered equal if the base iterators coincide.
     *
     * \param it Underlying base iterator
     * \param index Initial index
     * \param itStride On incrementing the iterator the base iterator is incremented itStride times.
     * \param indexStride On incrementing the iterator the index is incremented indexStride times.
     */
    IndexedSliceIterator(const BaseIterator& it, size_type index, size_type itStride=1, size_type indexStride=1) :
        it_(it),
        itStride_(itStride),
        index_(index),
        indexStride_(indexStride)
    {}

    typename Base::Reference dereference() const
    {
        return *it_;
    }

    bool equals(const IndexedSliceIterator& other) const
    {
        if (itStride_==0)
            return (index_ == other.index_);
        else
            return (it_ == other.it_);
    }

    void increment()
    {
        index_ += indexStride_;
        for(size_type i=0; i != itStride_; ++i)
            ++it_;
    }

    /**
     * \brief Get index of iterator.
     *
     * While the indices are strictly increasing but not necessarily
     * consecutive.
     */
    size_type index() const
    {
        return index_;
    }

protected:
    BaseIterator it_;
    size_type itStride_;
    size_type index_;
    size_type indexStride_;
};

#endif
