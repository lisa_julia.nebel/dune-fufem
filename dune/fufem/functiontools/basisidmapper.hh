#ifndef BASIS_ID_MAPPER_HH
#define BASIS_ID_MAPPER_HH

#include <array>
#include <memory>

#include <dune/common/fvector.hh>

#include <dune/istl/bvector.hh>

#include <dune/localfunctions/common/localkey.hh>
#include <dune/localfunctions/common/virtualwrappers.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functionspacebases/functionspacebasis.hh>

// forward declaration
template<class Mapper, class GridView, class C>
class BasisIdGridFunction;

/**
 * \brief This class map entities ids to DOFs w.r.t. a global basis
 *
 * This class maps the entities associated with the DOFs in a global basis
 * to the corresponding global indices.
 *
 * \tparam B Global function space basis type
 */
template<class B>
class BasisIdMapper
{
    private:
        typedef BasisIdMapper<B> MyType;

    public:
        typedef B Basis;
        typedef typename Basis::ReturnType ReturnType;
        typedef typename B::LocalFiniteElement LocalFiniteElement;
        typedef Dune::LocalFiniteElementCloneFactory<LocalFiniteElement> LfeCloneFactory;
        typedef typename B::BitVector BitVector;
        typedef typename B::LinearCombination LinearCombination;

        typedef typename Basis::GridView GridView;
        typedef typename GridView::Grid GridType;
        typedef typename GridType::template Codim<0>::Entity Element;

        typedef typename std::shared_ptr<LocalFiniteElement> SmartFEPointer;
        typedef typename std::map<const LocalFiniteElement*, SmartFEPointer > StoredFEMap;

        //! get domain dimension from the grid
        enum {dim=GridType::dimension};

        typedef typename GridType::Traits::GlobalIdSet IdSet;
        typedef typename IdSet::IdType IdType;

        /**
         * \brief Setup grid function
         *
         * \param basis Global function space basis
         * \param allowVanishingElements If this is set to true all elements are stored. This allows coarsening if elements vanish.
         *                               However this will in general only work for P1 and Q1 elements
         */
        BasisIdMapper(const B& basis, bool allowVanishingElements = false) :
            basis_(basis),
            gridview_(basis.getGridView()),
            allowVanishingElements_(allowVanishingElements)
        {
            update();
        }

        void update()
        {
            typedef typename B::LocalFiniteElement LocalFiniteElement;

            const GridView& gridview = basis_.getGridView();
            const auto& idSet = gridview_.grid().globalIdSet();

            storedFEs_.clear();
            elementToFE_.clear();
            for (size_t i=0; i<indices_.size(); i++)
                indices_[i].clear();

            for (auto&& e : elements(gridview)) {
                const IdType id = idSet.id(e);
                const LocalFiniteElement& fe = storeElement(id, e);

                // if elements might vanish we need to store all elements up to level 0
                if (allowVanishingElements_)
                    storeParents(e);

                for (size_t i=0; i<fe.localBasis().size(); ++i)
                {
                    const Dune::LocalKey& localKey = fe.localCoefficients().localKey(i);
                    const IdType id = gridview.grid().globalIdSet().subId(e, localKey.subEntity(), localKey.codim());
                    indices_[localKey.codim()][id] = basis_.index(e, i);
                }
            }
        }

        bool containsElement(const IdType& id) const
        {
            return elementToFE_.find(id) != elementToFE_.end();
        }

        const LocalFiniteElement& getLocalFiniteElement(const IdType& id) const
        {
            return *(storedFEs_.find(elementToFE_.find(id)->second)->second);
        }

        int index(const IdType& id, const int& codim) const
        {
            return indices_[codim].find(id)->second;
        }

        bool isConstrained(const int index) const
        {
            return basis_.isConstrained(index);
        }

        const LinearCombination& constraints(const int index) const
        {
            return basis_.constraints(index);
        }

        template<class GV, class C>
        BasisIdGridFunction<MyType, GV, C> makeFunction(const GV& gv, const C& v) const
        {
            return BasisIdGridFunction<MyType, GV, C>(*this, gv, v);
        }


    private:
        void storeFiniteElement(const LocalFiniteElement& fe)
        {
            // store local finite element if not already done
            // If we have a base class with virtual functions
            // we must use the 'virtual copy constructor' hack
            // using virtual clone() methods for ALL local FEs
            if (storedFEs_.count(&fe) == 0)
                storedFEs_.emplace(&fe, SmartFEPointer(LfeCloneFactory::clone(fe)));
        }

        const LocalFiniteElement& storeElement(const IdType& elementId, const Element& element)
        {
            const LocalFiniteElement& fe = basis_.getLocalFiniteElement(element);
            storeFiniteElement(fe);
            elementToFE_[elementId] = &fe;
            return fe;
        }

        void storeParents(const Element& element)
        {
            const auto& idSet = gridview_.grid().globalIdSet();

            Element e = element;
            IdType prevId = idSet.id(element);
            while (e.level() > 0) {
                e = e.father();
                const IdType id = idSet.id(e);

                // Element is its own father. This can happen when UGGrid is used
                // with the setRefinementType(UGGrid::COPY) option.
                // We can skip this element, but still might have to visit "real"
                // father elements.
                if (id == prevId)
                    continue;

                // Father was already added. No need to visit rest of the hierarchy.
                if (elementToFE_.count(id) != 0)
                    break;

                prevId = id;
                storeElement(id, element);
            }
        }

        const Basis& basis_;
        const GridView gridview_;

        StoredFEMap storedFEs_;
        std::map<IdType, const LocalFiniteElement*> elementToFE_;
        std::array< std::map<IdType, int> , dim+1 >  indices_;

        bool allowVanishingElements_;
};



/**
 * \brief Grid function for old coefficient vector.
 *
 * This class provides a grid function for coefficient vector associated to a
 * basis before grid refinement.
 * Create a corresponding BasisIdMapper before refinement and hand it to the
 * constructor with a coefficient vector for the basis before refinement.
 *
 * This requires that the constrains and  local finite element information
 * did not change since the mapper was created ! Furthermore only functionspaces
 * with a single DOF per entity are supported.
 *
 * \tparam Mapper The BasisIdMapper to use.
 */
template<class Mapper, class GridView, class C>
class BasisIdGridFunction :
    public VirtualGridViewFunction<GridView, typename C::value_type>
{
    protected:
        typedef VirtualGridViewFunction<GridView, typename C::value_type> BaseType;

    public:

        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        // basis types
        typedef typename Mapper::LocalFiniteElement LocalFiniteElement;

        // grid types
        typedef typename GridView::Grid GridType;

        //! get domain dimension from the grid
        enum {dim=GridType::dimension};

        typedef typename GridType::template Codim<0>::Entity Element;

        typedef typename GridType::Traits::GlobalIdSet IdSet;
        typedef typename IdSet::IdType IdType;

        // input/output types

        //! The type of the coefficient vectors
        typedef C CoefficientVector;


        /**
         * \brief Setup grid function
         *
         * \param basis Global function space basis
         * \param coefficients Coefficient vector
         */
        BasisIdGridFunction(const Mapper& mapper, const GridView& gridView, const CoefficientVector& coefficients) :
            VirtualGridViewFunction<GridView, typename C::value_type>(gridView),
            grid_(gridView.grid()),
            mapper_(mapper),
            coefficients_(coefficients)
        {}



        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            typedef typename Mapper::LocalFiniteElement::Traits::LocalBasisType::Traits::RangeType LBRangeType;
            typedef typename Mapper::LinearCombination LinearCombination;

            Element father = e;

            while(!mapper_.containsElement(grid_.globalIdSet().id(father)))
            {
                assert(father.level()>0);
                father = father.father();
            }

            const LocalFiniteElement& fe = mapper_.getLocalFiniteElement(grid_.globalIdSet().id(father));

            const DomainType globalX = e.geometry().global(x);
            const LocalDomainType evalLocalX = father.geometry().local(globalX);

            std::vector<LBRangeType> values;

            fe.localBasis().evaluateFunction(evalLocalX, values);

            y = 0;
            for (size_t i=0; i<fe.localBasis().size(); ++i)
            {
                const Dune::LocalKey& localKey = fe.localCoefficients().localKey(i);
                const IdType id = grid_.globalIdSet().subId(father, localKey.subEntity(), localKey.codim());
                int index = mapper_.index(id, localKey.codim());

                if (mapper_.isConstrained(index))
                {
                    const LinearCombination& constraints = mapper_.constraints(index);
                    for (size_t w=0; w<constraints.size(); ++w)
                        Dune::MatrixVector::addProduct(y, values[i] * constraints[w].factor, coefficients_[constraints[w].index]);
                }
                else
                    Dune::MatrixVector::addProduct(y, values[i], coefficients_[index]);
            }
        }


    private:

        const GridType& grid_;

        const Mapper& mapper_;
        const CoefficientVector& coefficients_;
};

#endif

