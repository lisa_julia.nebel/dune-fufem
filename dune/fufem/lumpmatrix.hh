#ifndef DUNE_LUMP_MATRIX_HH
#define DUNE_LUMP_MATRIX_HH

/** \file
    \brief Contains a method which lumps a BCRSMatrix
*/

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bdmatrix.hh>

    /** \brief Lump a sparse matrix */
    template <class BlockType>
    void lumpMatrix(const Dune::BCRSMatrix<BlockType>& inMatrix, 
                    Dune::BDMatrix<BlockType>& outMatrix)
    {
        if (inMatrix.N() != inMatrix.M())
            DUNE_THROW(Dune::ISTLError, "Can only lump quadratic matrices!");

        typedef typename Dune::BCRSMatrix<BlockType>::row_type RowType;
        typedef typename RowType::ConstIterator ColumnIterator;
        
        // resize out matrix
        //outMatrix.resize(inMatrix.N());
        outMatrix = Dune::BDMatrix<BlockType>(inMatrix.N());

        for (size_t i=0; i<inMatrix.N(); i++) {
            
            outMatrix[i] = 0;
            
            const RowType& row = inMatrix[i];
            
            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();
            
            for (; cIt!=cEndIt; ++cIt)
                outMatrix[i][i] += *cIt;
            
        }
        
    }

#endif
