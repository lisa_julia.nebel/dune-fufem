// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef INTERSECTION_OPERATOR_ASSEMBLER_HH
#define INTERSECTION_OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/matrix-vector/axpy.hh>

// TODO Modify such that the global assembler also works for nonconforming grids!

//! Global Assembler for operators that are defined on the intersections of the grid
template <class TestBasis, class AnsatzBasis>
class IntersectionOperatorAssembler
{

    private:
        typedef typename TestBasis::GridView GridView;

    public:
        //! create assembler for grid
        IntersectionOperatorAssembler(const TestBasis& tBasis, const AnsatzBasis& aBasis) :
            tBasis_(tBasis),
            aBasis_(aBasis)
        {}


        template <class LocalAssemblerType>
        void addIndices(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices, const bool lumping=false) const
        {
            if (lumping)
                addIndicesStaticLumping<LocalAssemblerType,true>(localAssembler, indices);
            else
                addIndicesStaticLumping<LocalAssemblerType,false>(localAssembler, indices);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void addEntries(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            if (lumping)
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,true>(localAssembler, A);
            else
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,false>(localAssembler, A);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void assemble(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            Dune::MatrixIndexSet indices(rows, cols);

            addIndices(localAssembler, indices, lumping);

            indices.exportIdx(A);
            A=0.0;

            addEntries(localAssembler, A, lumping);

            return;
        }


    protected:

        template <class LocalAssemblerType, bool lumping>
        void addIndicesStaticLumping(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices) const
        {
            typedef typename LocalAssemblerType::BoolMatrix BoolMatrix;
            typedef typename TestBasis::LinearCombination LinearCombination;


            // only handle every face one time
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView> faceMapper(tBasis_.getGridView(), mcmgLayout(Dune::Codim<1>()));
            std::vector<bool> handled(faceMapper.size(),false);

            for (const auto& e : elements(tBasis_.getGridView()))
            {

                const auto& tFEinside = tBasis_.getLocalFiniteElement(e);
                const auto& aFEinside = aBasis_.getLocalFiniteElement(e);

                for (const auto& is : intersections(tBasis_.getGridView(),e)) {

                    if (handled[faceMapper.subIndex(e,is.indexInInside(),1)])
                        continue;

                    handled[faceMapper.subIndex(e,is.indexInInside(),1)]=true;

                    //if the face lies on the boundary
                    if (is.boundary())
                    {

                        BoolMatrix localIndices(tFEinside.localBasis().size(), aFEinside.localBasis().size());

                        localAssembler.indices(is, localIndices, tFEinside, aFEinside);

                        for (size_t i=0; i<tFEinside.localBasis().size(); ++i)
                        {
                            int rowIndex= tBasis_.index(e, i);
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);

                            for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
                            {
                                if (localIndices[i][j])
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                        }
                                        else
                                            indices.add(rowIndex, rowIndex);
                                    }
                                    else
                                    {
                                        int colIndex= aBasis_.index(e, j);
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                        if (rowIsConstrained and colIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            {
                                                for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                    indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                            }
                                        }
                                        else if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, colIndex);
                                        }
                                        else if (colIsConstrained)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                indices.add(rowIndex, colConstraints[cw].index);
                                        }
                                        else
                                            indices.add(rowIndex, colIndex);
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        //local basis of the outside() element
                        const auto& outside = is.outside();
                        const auto& tFEoutside = tBasis_.getLocalFiniteElement(outside);
                        const auto& aFEoutside = aBasis_.getLocalFiniteElement(outside);

                        BoolMatrix localIndices(tFEinside.localBasis().size()+tFEoutside.localBasis().size(), aFEinside.localBasis().size()+aFEoutside.localBasis().size());

                        localAssembler.indices(is, localIndices, tFEinside, aFEinside,  tFEoutside, aFEoutside);

                        int rowIndex = 0;
                        int colIndex = 0;
                        for (size_t i=0; i<tFEinside.localBasis().size()+tFEoutside.localBasis().size(); ++i)
                        {
                            if (i<tFEinside.localBasis().size())
                                rowIndex= tBasis_.index(e, i);
                            else
                                rowIndex = tBasis_.index(outside,i-tFEinside.localBasis().size());
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size()+aFEoutside.localBasis().size(); ++j)
                            {
                                if (localIndices[i][j])
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                        }
                                        else
                                            indices.add(rowIndex, rowIndex);
                                    }
                                    else
                                    {
                                        if (j<tFEinside.localBasis().size())
                                            colIndex= aBasis_.index(e, j);
                                        else
                                            colIndex = aBasis_.index(outside,j-aFEinside.localBasis().size());
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                   if (rowIsConstrained and colIsConstrained)
                                   {
                                       for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                       {
                                           for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                               indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                       }
                                   }
                                   else if (rowIsConstrained)
                                   {
                                       for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                           indices.add(rowConstraints[rw].index, colIndex);
                                   }
                                   else if (colIsConstrained)
                                   {
                                       for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                           indices.add(rowIndex, colConstraints[cw].index);
                                   }
                                   else
                                       indices.add(rowIndex, colIndex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        template <class LocalAssemblerType, class GlobalMatrixType, bool lumping>
        void addEntriesStaticLumping(LocalAssemblerType& localAssembler, GlobalMatrixType& A) const
        {
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;
            typedef typename TestBasis::LinearCombination LinearCombination;

            // only handle every face one time
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView> faceMapper(tBasis_.getGridView(),mcmgLayout(Dune::Codim<1>()));
            std::vector<bool> handled(faceMapper.size(),false);

            for (const auto& e : elements(tBasis_.getGridView()))
            {

                const auto& tFEinside = tBasis_.getLocalFiniteElement(e);
                const auto& aFEinside = aBasis_.getLocalFiniteElement(e);

                for (const auto& is : intersections(tBasis_.getGridView(),e)) {

                    if (handled[faceMapper.subIndex(e,is.indexInInside(),1)])
                        continue;

                    handled[faceMapper.subIndex(e,is.indexInInside(),1)]=true;

                    //if the face lies on the boundary
                    if (is.boundary())
                    {

                        LocalMatrix localA(tFEinside.localBasis().size(), aFEinside.localBasis().size());
                        localAssembler.assemble(is, localA, tFEinside, aFEinside);

                        for (size_t i=0; i<tFEinside.localBasis().size(); ++i)
                        {
                            int rowIndex = tBasis_.index(e, i);
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size(); ++j)
                            {

                                if (localA[i][j].infinity_norm()!=0.0)
                                {
                                    if (lumping)
                                    {
                                        if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                        }
                                        else
                                            Dune::MatrixVector::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                                    }
                                    else
                                    {
                                        int colIndex = aBasis_.index(e, j);
                                        const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                        bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                        if (rowIsConstrained and colIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            {
                                                for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                    Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                            }
                                        }
                                        else if (rowIsConstrained)
                                        {
                                            for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                                Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                        }
                                        else if (colIsConstrained)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                Dune::MatrixVector::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                        }
                                        else
                                            Dune::MatrixVector::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                                    }
                                }
                            }
                        }

                    }//if face is not part of the boundary
                    else
                    {

                        //local basis of the outside() element
                        const auto& outside = is.outside();
                        const auto& tFEoutside = tBasis_.getLocalFiniteElement(outside);
                        const auto& aFEoutside = aBasis_.getLocalFiniteElement(outside);

                        LocalMatrix localA(tFEinside.localBasis().size()+tFEoutside.localBasis().size(), aFEinside.localBasis().size()+aFEoutside.localBasis().size());
                        localAssembler.assemble(is, localA, tFEinside, aFEinside,  tFEoutside, aFEoutside);

                        int rowIndex = 0;
                        int colIndex = 0;

                        //when using this global face assembler with continuous finite element spaces some
                        //local basis functions are part of the same global basis function.
                        //thus the we have to neglect the entries of the local matrix that
                        //belong to the same pairing of global basis functions we already added
                        std::set<std::pair<int,int> > sameGlobalPair;

                        for (size_t i=0; i<tFEinside.localBasis().size()+tFEoutside.localBasis().size(); ++i)
                        {
                            if (i<tFEinside.localBasis().size())
                                rowIndex= tBasis_.index(e, i);
                            else
                                rowIndex = tBasis_.index(outside,i-tFEinside.localBasis().size());
                            const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                            bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                            for (size_t j=0; j<aFEinside.localBasis().size()+aFEoutside.localBasis().size(); ++j)
                            {
                                if (localA[i][j].infinity_norm()==0.0)
                                    continue;

                                if (j<aFEinside.localBasis().size())
                                    colIndex= aBasis_.index(e, j);
                                else
                                    colIndex = aBasis_.index(outside,j-aFEinside.localBasis().size());

                                //if (i,j) has the same global index pair as one that has already been added
                                //to the global matrix then dismiss it
                                if (sameGlobalPair.find(std::pair<int,int>(rowIndex,colIndex)) != sameGlobalPair.end())
                                    continue;
                                else
                                    sameGlobalPair.insert(std::pair<int,int>(rowIndex,colIndex));

                                if (lumping)
                                {
                                    if (rowIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                    else
                                        Dune::MatrixVector::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                                }
                                else
                                {

                                    const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                    bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                    if (rowIsConstrained and colIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        {
                                            for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                                Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                        }
                                    }
                                    else if (rowIsConstrained)
                                    {
                                        for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                            Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                    else if (colIsConstrained)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            Dune::MatrixVector::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                    }
                                    else
                                        Dune::MatrixVector::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                                }
                            }
                        }
                    }
                }
            }
        }

        const TestBasis& tBasis_;
        const AnsatzBasis& aBasis_;
};

#endif

