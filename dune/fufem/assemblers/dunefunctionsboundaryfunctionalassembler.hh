// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSBOUNDARYFUNCTIONALASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSBOUNDARYFUNCTIONALASSEMBLER_HH

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/boundarypatch.hh"


namespace Dune {
namespace Fufem {


//! Generic global assembler for functionals on a boundary
template <class TestBasis>
class DuneFunctionsBoundaryFunctionalAssembler
{
  using GridView = typename TestBasis::GridView;

public:
  //! create assembler for grid
  DuneFunctionsBoundaryFunctionalAssembler(const TestBasis& tBasis, const BoundaryPatch<GridView>& boundaryPatch) :
      testBasis_(tBasis),
      boundaryPatch_(boundaryPatch)
  {}



  template <class VectorBackend, class LocalAssembler>
  void assembleBulkEntries(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler) const
  {
    auto testLocalView     = testBasis_.localView();

    using Field = std::decay_t<decltype(vectorBackend[testLocalView.index(0)])>;
    using LocalVector = Dune::BlockVector<Dune::FieldVector<Field,1>>;

    auto localVector = LocalVector(testLocalView.maxSize());

    // iterate over boundary
    for (auto it =boundaryPatch_.begin(); it!= boundaryPatch_.end(); ++it)
    {
      const auto& element = it->inside();
      testLocalView.bind(element);

      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        localVector[localRow] = 0;
      }

      localAssembler(it, localVector, testLocalView);

      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        auto row = testLocalView.index(localRow);
        vectorBackend[row] += localVector[localRow];
      }
    }
  }

  template <class VectorBackend, class LocalAssembler>
  void assembleBulk(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler) const
  {
    vectorBackend.resize(testBasis_);
    vectorBackend.vector() = 0.0;
    assembleBulkEntries(vectorBackend, std::forward<LocalAssembler>(localAssembler));
  }


protected:
  const TestBasis& testBasis_;
  const BoundaryPatch<GridView>& boundaryPatch_;
};


/**
 * \brief Create DuneFunctionsBoundaryFunctionalAssembler
 */
template <class TestBasis, class BP>
auto duneFunctionsBoundaryFunctionalAssembler(const TestBasis& testBasis, const BP& bp)
{
  return DuneFunctionsBoundaryFunctionalAssembler<TestBasis>(testBasis, bp);
}



} // namespace Fufem
} // namespace Dune


#endif

