#ifndef PRECONDITIONED_DEFECT_ASSEMBLER_HH
#define PRECONDITIONED_DEFECT_ASSEMBLER_HH

#include <array>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"

//! \todo Please doc me!
template <class ExtensionBasisType, class ExtendedBasisType>
class PreconditionedDefectAssembler
{
    private:
        typedef typename ExtendedBasisType::GridView GridView;

    public:
        //! create assembler for grid
        PreconditionedDefectAssembler(const ExtensionBasisType& extensionBasis, const ExtendedBasisType& extendedBasis) :
            extensionBasis_(extensionBasis),
            extendedBasis_(extendedBasis)
        {}


        template <class LocalAssemblerType, class GlobalMatrixType, class GlobalRHSVectorType, class GlobalLHSVectorType>
        void assemble(
                LocalAssemblerType& localAssembler,
                GlobalMatrixType& A,
                GlobalRHSVectorType& b,
                const double alpha,
                const GlobalLHSVectorType& x,
                const bool lumping=false) const
        {
            std::array<GlobalRHSVectorType*,1> b_wrapped = {{ &b }};
            std::array<const double,1> alpha_wrapped = {{ alpha}};
            std::array<const GlobalLHSVectorType*,1> x_wrapped = {{ &x }};

            assembleMultipleResiduals(localAssembler, A, b_wrapped, alpha_wrapped, x_wrapped, lumping);
        }

        template <class LocalAssemblerType, class GlobalMatrixType, class VectorOfGlobalVectorType, class VectorOfCoefficients, class ConstVectorOfGlobalVectorType>
        void assembleMultipleResiduals(
                LocalAssemblerType& localAssembler,
                GlobalMatrixType& A,
                VectorOfGlobalVectorType& b,
                const VectorOfCoefficients& alpha,
                const ConstVectorOfGlobalVectorType& x,
                const bool lumping=false) const
        {
            int size = extendedBasis_.size();
//            int size = extensionBasis_.size();

            A.setSize(size,size,size);
            A.setBuildMode(GlobalMatrixType::row_wise);
            typename GlobalMatrixType::CreateIterator it = A.createbegin();
            for(; it!=A.createend(); ++it)
//                if (not(extensionBasis_.isConstrained(it.index())))
                    it.insert(it.index());

            A=0.0;

            if (lumping)
                assembleStaticLumping<
                    LocalAssemblerType,
                    GlobalMatrixType,
                    VectorOfGlobalVectorType,
                    VectorOfCoefficients,
                    ConstVectorOfGlobalVectorType,
                    true>
                    (localAssembler, A, b, alpha, x);
            else
                assembleStaticLumping<
                    LocalAssemblerType,
                    GlobalMatrixType,
                    VectorOfGlobalVectorType,
                    VectorOfCoefficients,
                    ConstVectorOfGlobalVectorType,
                    false>
                    (localAssembler, A, b, alpha, x);
        }


    private:

        template <class LocalAssemblerType, class GlobalMatrixType, class VectorOfGlobalVectorType, class VectorOfCoefficients, class ConstVectorOfGlobalVectorType, bool lumping>
        void assembleStaticLumping(
                LocalAssemblerType& localAssembler,
                GlobalMatrixType& A,
                VectorOfGlobalVectorType& b,
                const VectorOfCoefficients& alpha,
                const ConstVectorOfGlobalVectorType& x) const
        {
            typedef typename GridView::template Codim<0>::Iterator ElementIterator;
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;
            typedef typename ExtensionBasisType::LinearCombination LinearCombination;

            ElementIterator it = extendedBasis_.getGridView().template begin<0>();
            ElementIterator end = extendedBasis_.getGridView().template end<0>();
            for (; it != end; ++it)
            {
                // get shape functions
                const typename ExtendedBasisType::LocalFiniteElement& FE = extendedBasis_.getLocalFiniteElement(*it);

                LocalMatrix localA(FE.localBasis().size(), FE.localBasis().size());
                localAssembler.assemble(*it, localA, FE, FE);

                for (size_t i=0; i<FE.localBasis().size(); ++i)
                {
                    // We use  the extension basis for each row
                    int rowIndex = extendedBasis_.index(*it, i);
                    const LinearCombination& rowConstraints = extendedBasis_.constraints(rowIndex);
                    bool rowIsConstrained = extendedBasis_.isConstrained(rowIndex);
                    for (size_t j=0; j<FE.localBasis().size(); ++j)
                    {
                        if (localA[i][j].infinity_norm()!=0.0)
                        {
                            if (lumping)
                            {
                                if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t k=0; k<alpha.size(); ++k)
                                            localA[i][j].usmv(-rowConstraints[rw].factor*alpha[k], (*x[k])[rowConstraints[rw].index], (*b[k])[rowConstraints[rw].index]);
//                                        A[rowConstraints[rw].index][rowConstraints[rw].index].axpy(rowConstraints[rw].factor, localA[i][j]);
                                        Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                }
                                else
                                {
                                    for (size_t k=0; k<alpha.size(); ++k)
                                        localA[i][j].usmv(-alpha[k], (*x[k])[rowIndex], (*b[k])[rowIndex]);
//                                    A[rowIndex][rowIndex].axpy(1.0, localA[i][j]);
                                    Dune::MatrixVector::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                                }
                            }
                            else
                            {
                                int colIndex = extendedBasis_.index(*it, j);
                                const LinearCombination& colConstraints = extendedBasis_.constraints(colIndex);
                                bool colIsConstrained = extendedBasis_.isConstrained(colIndex);
                                if (rowIsConstrained and colIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        {
                                            for (size_t k=0; k<alpha.size(); ++k)
                                                localA[i][j].usmv(
                                                        -rowConstraints[rw].factor * alpha[k] * colConstraints[cw].factor,
                                                        (*x[k])[colConstraints[cw].index],
                                                        (*b[k])[rowConstraints[rw].index]);
                                            if (rowConstraints[rw].index == colConstraints[cw].index)
//                                                A[rowConstraints[rw].index][colConstraints[cw].index].axpy(rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                                Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                        }
                                    }
                                }
                                else if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t k=0; k<alpha.size(); ++k)
                                            localA[i][j].usmv(-rowConstraints[rw].factor*alpha[k], (*x[k])[colIndex], (*b[k])[rowConstraints[rw].index]);
                                        if (rowConstraints[rw].index == colIndex)
//                                            A[rowConstraints[rw].index][colIndex].axpy(rowConstraints[rw].factor, localA[i][j]);
                                            Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                    }
                                }
                                else if (colIsConstrained)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                    {
                                        for (size_t k=0; k<alpha.size(); ++k)
                                            localA[i][j].usmv(-colConstraints[cw].factor*alpha[k], (*x[k])[colConstraints[cw].index], (*b[k])[rowIndex]);
                                        if (rowIndex == colConstraints[cw].index)
//                                            A[rowIndex][colConstraints[cw].index].axpy(colConstraints[cw].factor, localA[i][j]);
                                            Dune::MatrixVector::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                    }
                                }
                                else
                                {
                                    for (size_t k=0; k<alpha.size(); ++k)
                                        localA[i][j].usmv(-alpha[k], (*x[k])[colIndex], (*b[k])[rowIndex]);
                                    if (rowIndex == colIndex)
//                                        A[rowIndex][colIndex].axpy(1.0, localA[i][j]);
                                        Dune::MatrixVector::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                                }
                            }

                        }
                    }
                }
            }
        }


        const ExtensionBasisType& extensionBasis_;
        const ExtendedBasisType& extendedBasis_;
};

#endif

