// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_OPERATOR_ASSEMBLER_HH
#define DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_OPERATOR_ASSEMBLER_HH

#include <array>
#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/matrix-vector/addtodiagonal.hh>

/** \brief Assemble the second derivative of the geometrically exact St.-Venant-Kirchhoff material
 *
 *  \f[
 *      J(u) = \int_{\Omega} E(u):C:E(u) dx
 *  \f]
 *
 *  which results in the bilinearform
 *
 *    \f[
 *      a_u(v,w) = \int_{\Omega} \sigma(u)E''_u(v,w) + (C:E'_u(v)):E'_u(w) dx
 *    \f]
 *   with
 *      - \f$D\varphi\f$: the deformation gradient
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$E'_u(v)\f$: the linearized strain tensor at the point u in direction v
 *      - \f$\sigma(u) = C:E(u)\f$: the second Piola-Kirchhoff stress tensor
 *      - \f$C\f$: the Hooke tensor
 *      - \f$Dv\f$: the gradient of the test function
 *
 */
template <class GridType, class TestLocalFE, class AnsatzLocalFE>
class GeomExactStVenantKirchhoffOperatorAssembler
    : public LocalOperatorAssembler < GridType, TestLocalFE, AnsatzLocalFE, Dune::FieldMatrix<typename GridType::ctype ,GridType::dimension,GridType::dimension> >
{


    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    using field_type = typename AnsatzLocalFE::Traits::LocalBasisType::Traits::RangeFieldType;

    typedef VirtualGridFunction<GridType, Dune::FieldVector<ctype, dim> > GridFunction;

    typedef Dune::FieldMatrix<field_type, dim, dim> T;
    using Base = LocalOperatorAssembler < GridType, TestLocalFE, AnsatzLocalFE, T >;

public:
    using MatrixEntry = typename Base::MatrixEntry;
    using Element = typename Base::Element;
    using BoolMatrix = typename Base::BoolMatrix;
    using LocalMatrix = typename Base::LocalMatrix;

   GeomExactStVenantKirchhoffOperatorAssembler(ctype E, ctype nu, std::shared_ptr<GridFunction> configuration)
        : E_(E), nu_(nu), configuration_(configuration)
    {}

    GeomExactStVenantKirchhoffOperatorAssembler(ctype E, ctype nu) :
        E_(E), nu_(nu)
    {}

    void indices(const Element& element, BoolMatrix& isNonZero, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    void assemble(const Element& element, LocalMatrix& localMatrix, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {

        int rows = tFE.localBasis().size();
        int cols = aFE.localBasis().size();

        localMatrix.setSize(rows,cols);
        localMatrix = 0.0;

        const int order = (element.type().isSimplex())
            ? (tFE.localBasis().order())
            : (tFE.localBasis().order()*dim);
        const auto& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TestLocalFE>::value(tFE) );

        using ReferenceLfeJacobian = typename TestLocalFE::Traits::LocalBasisType::Traits::JacobianType;
        std::vector<ReferenceLfeJacobian> referenceGradients(tFE.localBasis().size());

        const auto& geometry = element.geometry();

        for (size_t pt=0; pt < quad.size(); ++pt) {

            const auto& quadPos = quad[pt].position();

            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            using GlobalLfeJacobian = Dune::FieldVector<ctype, dim>;
            std::vector<GlobalLfeJacobian> gradients(tFE.localBasis().size());
            for (size_t i=0; i < gradients.size(); ++i) {
                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);
            }

            using Jacobian = typename GridFunction::DerivativeType;
            Jacobian localConfGrad;
            if (configuration_->isDefinedOn(element))
                configuration_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                configuration_->evaluateDerivative(geometry.global(quadPos), localConfGrad);

            SymmetricTensor<dim, field_type> strain;
            computeStrain(localConfGrad, strain);
            SymmetricTensor<dim, field_type> stress = hookeTimesStrain(strain);

            Dune::MatrixVector::addToDiagonal(localConfGrad, 1.0);

            std::vector<std::array<Jacobian, dim > > defJac(rows);
            std::vector<std::array<SymmetricTensor<dim>,dim> > linStrain(rows);

            for (int i=0; i<rows; i++) {

                for (int k=0; k<dim; k++) {

                    defJac[i][k] = 0;
                    defJac[i][k][k] = gradients[i];

                    // The linearized strain is the symmetric product of defGrad and (Id+localConf)
                    linStrain[i][k]=symmetricProduct(defJac[i][k], localConfGrad);
                }
            }

            const auto integrationElement = geometry.integrationElement(quadPos);
            ctype z = quad[pt].weight() * integrationElement;
            for (int row=0; row < rows; row++)
                for (int rcomp=0; rcomp<dim; rcomp++) {

                    auto linStress = hookeTimesStrain(linStrain[row][rcomp]);

                    for (int col=0; col < cols; col++)
                        for (int ccomp=0; ccomp < dim; ccomp++) {
                            localMatrix[row][col][rcomp][ccomp] += (linStress*linStrain[col][ccomp])*z;
                            localMatrix[row][col][rcomp][ccomp] += (stress*symmetricProduct(defJac[row][rcomp], defJac[col][ccomp]))*z;
                        }
                }
        }
    }

    /** \brief Set new configuration to be assembled at. Needed e.g. during Newton iteration.*/
    void setConfiguration(std::shared_ptr<GridFunction> newConfig) {
        configuration_ = newConfig;
        if (!configuration_)
            DUNE_THROW(Dune::Exception,"In [GeomExactStVenantKirchhoffOperatorAssembler]: You need to provide a GridFunction describing the displacement!");
    }

protected:
    /** \brief Young's modulus */
    ctype E_;

    /** \brief The Poisson ratio */
    ctype nu_;

    /** \brief The configuration at which the linearized operator is evaluated.*/
    std::shared_ptr<GridFunction> configuration_;

    /** \brief Compute nonlinear strain tensor from the deformation gradient. */
    void computeStrain(const Dune::FieldMatrix<ctype, dim, dim>& grad, SymmetricTensor<dim>& strain) const {
        strain = 0;
        for (int i=0; i<dim ; ++i) {
            strain(i,i) +=grad[i][i];

            for (int k=0;k<dim;++k)
                strain(i,i) += 0.5*grad[k][i]*grad[k][i];

            for (int j=i+1; j<dim; ++j) {
                strain(i,j) += 0.5*(grad[i][j] + grad[j][i]);
                for (int k=0;k<dim;++k)
                    strain(i,j) += 0.5*grad[k][i]*grad[k][j];
            }
        }
    }

    /** \brief Compute the stress tensor from the nonlinear strain tensor. */
    SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {

        SymmetricTensor<dim> stress = strain;
        stress *= E_/(1+nu_);

        ctype f = E_*nu_ / ((1+nu_)*(1-2*nu_)) * strain.trace();

        stress.addToDiag(f);

        return stress;
    }

    /** \brief Compute the symmetric product of two matrices, i.e.
     *      0.5*(A^T * B + B^T * A )
     */
    SymmetricTensor<dim> symmetricProduct(const Dune::FieldMatrix<ctype, dim, dim>& a, const Dune::FieldMatrix<ctype, dim, dim>& b) const {

        SymmetricTensor<dim> result(0.0);
        for (int i=0;i<dim; i++)
            for (int j=i;j<dim;j++)
                for (int k=0;k<dim;k++)
                    result(i,j)+= a[k][i]*b[k][j]+b[k][i]*a[k][j];
        result *= 0.5;

        return result;
    }

};


#endif

