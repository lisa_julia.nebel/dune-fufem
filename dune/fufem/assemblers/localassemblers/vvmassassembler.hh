#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVMASSASSEMBLER
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVMASSASSEMBLER

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>

//** \brief Local mass assembler **//
template <class GridType, class TestLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class VVMassAssembler : public LocalOperatorAssembler < GridType, TestLocalFE, AnsatzLocalFE, T >
{
  private:
    typedef LocalOperatorAssembler < GridType, TestLocalFE, AnsatzLocalFE ,T > Base;
    static const int dim = GridType::dimension;

  public:
    typedef typename Base::Element Element;
    typedef typename Element::Geometry Geometry;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;
  private:
    const T& one_;

  public:
    VVMassAssembler(const T& one)
      : one_(one)
    {}

    void indices([[maybe_unused]] const Element& element, BoolMatrix& isNonZero, [[maybe_unused]] const TestLocalFE& tFE, [[maybe_unused]] const AnsatzLocalFE& aFE) const
    {
      isNonZero = true;
    }

    template <class BoundaryIterator>
    void indices([[maybe_unused]] const BoundaryIterator& it, BoolMatrix& isNonZero, [[maybe_unused]] const TestLocalFE& tFE, [[maybe_unused]] const AnsatzLocalFE& aFE) const
    {
      isNonZero = true;
    }

    void assemble(const Element& element, LocalMatrix& localMatrix, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
      typedef typename Dune::template FieldVector<double,dim> FVdim;
      typedef typename TestLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

      // Make sure we got suitable shape functions
      assert(tFE.type() == element.type());
      assert(aFE.type() == element.type());

      // check if ansatz local fe = test local fe
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

      int rows = localMatrix.N();
      int cols = localMatrix.M();

      // get geometry and store it
      const Geometry geometry = element.geometry();

      localMatrix = 0.0;

      // get quadrature rule
      QuadratureRuleKey quadKey = QuadratureRuleKey(tFE).product(QuadratureRuleKey(aFE));
      const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

      // store values of shape functions
      std::vector<RangeType> values(tFE.localBasis().size());

      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // get quadrature point
        const FVdim& quadPos = quad[pt].position();

        // get integration factor
        const double integrationElement = geometry.integrationElement(quadPos);

        // evaluate basis functions
        tFE.localBasis().evaluateFunction(quadPos, values);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for(int i=0; i<rows; ++i)
        {
          double zi = values[i]*z;

          for (int j=i+1; j<cols; ++j)
          {
            double zij = values[j] * zi;
            Dune::MatrixVector::addProduct(localMatrix[i][j], zij, one_);
            Dune::MatrixVector::addProduct(localMatrix[j][i], zij, one_);
          }
          Dune::MatrixVector::addProduct(localMatrix[i][i], values[i] * zi, one_);
        }
      }
    }


    /** \brief Assemble the local mass matrix for a given boundary face
         */
    template <class BoundaryIterator>
    void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
      typedef typename BoundaryIterator::Intersection::Geometry Geometry;
      typedef typename TestLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

      // Make sure we got suitable shape functions
      assert(tFE.type() == it->inside()->type());
      assert(aFE.type() == it->inside()->type());

      // check if ansatz local fe = test local fe
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

      int rows = localMatrix.N();
      int cols = localMatrix.M();

      // get geometry and store it
      const Geometry intersectionGeometry = it->geometry();

      localMatrix = 0.0;

      // get quadrature rule
      QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
      QuadratureRuleKey quadKey = tFEquad.square();

      const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

      // store values of shape functions
      std::vector<RangeType> values(tFE.localBasis().size());

      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // get quadrature point
        const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

        // get integration factor
        const double integrationElement = intersectionGeometry.integrationElement(quadPos);

        // get values of shape functions
        tFE.localBasis().evaluateFunction(it->geometryInInside().global(quadPos), values);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for (int i=0; i<rows; ++i)
        {
          for (int j=i+1; j<cols; ++j)
          {
            double zij = values[i] * values[j] * z;
            localMatrix[i][j].axpy(zij, one_);
            localMatrix[j][i].axpy(zij, one_);
          }
          localMatrix[i][i].axpy(values[i] * values[i] * z, one_);
        }
      }

    }

};

#endif // DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVMASSASSEMBLER
