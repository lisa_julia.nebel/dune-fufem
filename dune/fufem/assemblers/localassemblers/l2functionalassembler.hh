// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef L2_FUNCTIONAL_ASSEMBLER_HH
#define L2_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/gridfunctionhelper.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>

/** \brief Local finite element assembler for L^2 functionals. */ 
template <class GridType, class TestLocalFE, class T=Dune::FieldVector<double,1> >
class L2FunctionalAssembler :
    public LocalFunctionalAssembler<GridType, TestLocalFE, T>

{
    private:
        typedef LocalFunctionalAssembler<GridType, TestLocalFE, T> Base;
        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        using LocalFunction = Dune::Fufem::Impl::LocalFunctionInterfaceForGrid<GridType, T>;

    public:
        typedef typename Base::Element Element;
        typedef typename Base::Element::Geometry Geometry;
        typedef typename Base::LocalVector LocalVector;

        /**
         * \brief Create L2FunctionalAssembler
         *
         * Creates a local functional assembler for an L2-functional.
         * The QuadratureRuleKey given here does only specify what is
         * needed to integrate f. Depending on this the quadrature rule
         * is selected automatically such that fv for test functions v
         * can be integrated.
         * If you, e.g., specify quadrature order 1 for f and the test
         * functions need order 2 the selected quadrature rule will
         * have order 3.
         *
         * \param f The L2 function
         * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
         */
        template<class FT>
        L2FunctionalAssembler(const FT& f, const QuadratureRuleKey& fQuadKey) :
            localF_(Dune::Fufem::Impl::localFunctionForGrid<GridType, T>(f)),
            functionQuadKey_(fQuadKey)
        {}

        /**
         * \brief Create L2FunctionalAssembler
         *
         * Using this constructor the quadrature order will be selected
         * such that the test functions can be integrated exactly.
         *
         * \param f The L2 function
         */
        template<class FT>
        L2FunctionalAssembler(const FT& f) :
            localF_(Dune::Fufem::Impl::localFunctionForGrid<GridType, T>(f)),
            functionQuadKey_(dim, 0)
        {}

        virtual void assemble(const Element& element, LocalVector& localVector, const TestLocalFE& tFE) const
        {
            typedef typename TestLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.product(functionQuadKey_);
            const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            localF_.bind(element);

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const LocalCoordinate& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                // compute values of function
                T f_pos = localF_(quadPos);

                // and vector entries
                for(size_t i=0; i<values.size(); ++i)
                {
                    localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                }
            }
            return;
        }

    private:
        mutable LocalFunction localF_;
        const QuadratureRuleKey functionQuadKey_;
};

#endif

