// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef VON_MISES_STRESS_ASSEMBLER_HH
#define VON_MISES_STRESS_ASSEMBLER_HH

#include <cmath>
#include <memory>
#include <dune/common/fvector.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Computes the von Mises stress at the element center.
 *
 *  The von Mises stress is given by
 *
 *   sqrt(sigma_x^2 + sigma_y^2 + sigma_z^2 - sigma_x*sigma_y -sigma_x*sigma_z -sigma_y*sigma_z + 3*(tau_xy^2 + tau_xz^2 +tau_yz^2) )
 *
 * where
 *      sigma_* - the normal stresses
 *      tau_**  - the shear stresses
 *
 * To get the correct global distribution this assembler has to be used with P0 global basis!
 */
template <class GridType, class TestLocalFE>
class VonMisesStressAssembler :
    public LocalFunctionalAssembler<GridType, TestLocalFE, Dune::FieldVector<typename GridType::ctype, 1> >

{
    static const int dim = GridType::dimension;
    typedef typename Dune::FieldVector<typename GridType::ctype, 1> T;
    typedef LocalFunctionalAssembler<GridType, TestLocalFE, T> Base;
    using field_type = typename TestLocalFE::Traits::LocalBasisType::Traits::RangeFieldType;

public:
    typedef typename Base::Element Element;
    typedef typename Base::LocalVector LocalVector;
    typedef VirtualGridFunction<GridType, Dune::FieldVector<field_type, dim> > GridFunction;
    using Derivative = typename GridFunction::DerivativeType;
    using StressFunction = std::function<SymmetricTensor<dim, field_type>(Derivative)>;

    /**
     * \brief Create assembler for grid
     * \param E  Young's modulus
     * \param nu Poisson's ratio
     * \param displace The displacement field
     */
    VonMisesStressAssembler(field_type E, field_type nu, std::shared_ptr<GridFunction const> displace) :
        displace_(displace)
    {
        stress_ = linearElasticStressFunction(E, nu);
    }

    /**
     * \brief Create assembler for grid
     * \param displace The displacement field
     * \param stress Function that computes stresses vom displacement gradients
     */
    VonMisesStressAssembler(std::shared_ptr<GridFunction const> displace,
                            StressFunction stress) :
        displace_(displace), stress_(stress)
    {}

    /** \brief Compute function that maps displacement gradients to the linear elastic stress.*/
    static const StressFunction linearElasticStressFunction(field_type E, field_type nu) {
      field_type lambda{E * nu / (1.0+nu) / (1.0-2.0*nu)};
      field_type twoMu{E / (1+nu)};

      return [=](const Derivative& grad) {

          SymmetricTensor<dim, field_type> strain;
          for (int i=0; i<dim ; ++i) {
              strain(i,i) = grad[i][i];
              for (int j=i+1; j<dim; ++j)
                  strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
          }

          auto stress = strain;
          stress *= twoMu;
          stress.addToDiag(lambda * strain.trace());

          return stress;
      };
    }

    /** \brief Assemble the von Mises stress. Since it is not really a functional the test finite element is not used. */
    void assemble(const Element& element, LocalVector& localVector, const TestLocalFE& tFE) const
    {
        if (tFE.localBasis().size() != 1)
            DUNE_THROW(Dune::Exception, "Use P0Basis in the FunctionalAssembler or this will not return the correct P0 data!");

        localVector = 0.0;

        const typename Element::Geometry geometry = element.geometry();

        auto center = geometry.local(geometry.center());

        // evaluate the displacement gradient at the center of the element
        Derivative localDispGradient;
        if (displace_->isDefinedOn(element))
            displace_->evaluateDerivativeLocal(element, center, localDispGradient);
        else
            displace_->evaluateDerivative(geometry.center(), localDispGradient);

        localVector[0] = getVonMises(stress_(localDispGradient));
    }

private:

    /** \brief The displacement.*/
    std::shared_ptr<GridFunction const> const displace_;

    /** \brief Function that computes the stress from the displacement gradient.*/
    StressFunction stress_;

    /** \brief Compute the von Mises stress from the stress tensor. */
    field_type getVonMises(const SymmetricTensor<dim, field_type>& stress) const {

        field_type vonMises{0};

        for (int i=0; i<dim-1; i++)
            for (int j=i+1; j<dim; j++) {
                vonMises += (stress(i,i) - stress(j,j)) * (stress(i,i) - stress(j,j));
                vonMises += 6 * (stress(i,j) * stress(i,j));
            }

        return std::sqrt(vonMises / 2);
    }
};

#endif
