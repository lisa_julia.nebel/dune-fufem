#ifndef CONVOLUTION_ASSEMBLER_HH
#define CONVOLUTION_ASSEMBLER_HH

#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

// Borrow internal helper from dune-localfunctions
// for transition of function interface
#include <dune/localfunctions/common/localinterpolation.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/staticmatrixtools.hh"
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"



//** \brief Local mass assembler **//
template <class TestBasis, class AnsatzBasis, class T=Dune::FieldMatrix<double,1,1> >
class ConvolutionAssembler
{
    public:
        typedef typename TestBasis::LocalFiniteElement TestLocalFE;
        typedef typename AnsatzBasis::LocalFiniteElement AnsatzLocalFE;
        typedef typename TestBasis::GridView TestGridView;
        typedef typename AnsatzBasis::GridView AnsatzGridView;

        typedef typename AnsatzGridView::template Codim<0>::Entity AnsatzElement;
        typedef typename TestGridView::template Codim<0>::Entity TestElement;
        typedef typename AnsatzElement::Geometry AnsatzGeometry;
        typedef typename TestElement::Geometry TestGeometry;
        typedef typename Dune::Matrix<T> LocalMatrix;

        typedef typename Dune::template FieldVector<double,TestGridView::dimensionworld> TestGlobalCoordinate;
        typedef typename Dune::template FieldVector<double,AnsatzGridView::dimensionworld> AnsatzGlobalCoordinate;
        typedef typename std::template pair<TestGlobalCoordinate, AnsatzGlobalCoordinate> KernelArgument;

        typedef typename std::function<double(KernelArgument)> KernelFunction;

        template<class KFunc>
        ConvolutionAssembler(const KFunc& kernel, int tQuadOrder=2, int aQuadOrder=2):
            kernel_(Dune::Impl::makeFunctionWithCallOperator<KernelArgument>(kernel)),
            tQuadOrder_(tQuadOrder),
            aQuadOrder_(aQuadOrder)
        {}


        void assemble(const TestElement& tElement, const TestLocalFE& tFE, const AnsatzElement& aElement, const AnsatzLocalFE& aFE, LocalMatrix& localMatrix) const
        {
            static const int tDim = TestGridView::dimension;
            static const int aDim = AnsatzGridView::dimension;

            typedef typename Dune::template FieldVector<double,tDim> TestLocalCoordinate;
            typedef typename Dune::template FieldVector<double,aDim> AnsatzLocalCoordinate;

            typedef typename TestLocalFE::Traits::LocalBasisType::Traits::RangeType TestRangeType;
            typedef typename AnsatzLocalFE::Traits::LocalBasisType::Traits::RangeType AnsatzRangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == tElement.type());
            assert(aFE.type() == aElement.type());

            // get geometries and store them
            const TestGeometry tGeometry = tElement.geometry();
            const AnsatzGeometry aGeometry = aElement.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            const Dune::template QuadratureRule<double, tDim>& tQuad
                = QuadratureRuleCache<double, tDim>::rule(tElement.type(), tQuadOrder_, IsRefinedLocalFiniteElement<TestLocalFE>::value(tFE) );
            const Dune::template QuadratureRule<double, aDim>& aQuad
                = QuadratureRuleCache<double, aDim>::rule(aElement.type(), aQuadOrder_, IsRefinedLocalFiniteElement<AnsatzLocalFE>::value(aFE) );

            // store values of shape functions
            std::vector<TestRangeType> tValues(tFE.localBasis().size());
            std::vector<AnsatzRangeType> aValues(aFE.localBasis().size());

            // store value of kernel
            double kernelValue;

            // loop over test quadrature points
            for (size_t tPt=0; tPt < tQuad.size(); ++tPt)
            {
                // get quadrature point
                const TestLocalCoordinate& tQuadPos = tQuad[tPt].position();

                // get global coordinates of quadrature point
                const TestGlobalCoordinate tGlobalQuadPos = tGeometry.global(tQuadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(tQuadPos, tValues);

                // compute overall integration weight from quadrature weight and integration element
                double tIntegrationWeight = tQuad[tPt].weight() * tGeometry.integrationElement(tQuadPos);

                // loop over ansatz quadrature points
                for (size_t aPt=0; aPt < aQuad.size(); ++aPt)
                {
                    // get quadrature point
                    const AnsatzLocalCoordinate& aQuadPos = aQuad[aPt].position();

                    // get global coordinates of quadrature point
                    const AnsatzGlobalCoordinate aGlobalQuadPos = aGeometry.global(aQuadPos);

                    // evaluate basis functions
                    aFE.localBasis().evaluateFunction(aQuadPos, aValues);

                    // compute overall integration weight from quadrature weight and integration element
                    double aIntegrationWeight = aQuad[aPt].weight() * aGeometry.integrationElement(aQuadPos);


                    // evaluate integration kernel at quadrature points
                    kernelValue = kernel_(std::make_pair(tGlobalQuadPos, aGlobalQuadPos));

                    for (size_t i=0; i<tFE.localBasis().size(); ++i)
                    {
                        double tIntegral = tValues[i]*tIntegrationWeight;
                        for (size_t j=0; j<aFE.localBasis().size(); ++j)
                        {
                            double aIntegral = aValues[i]*aIntegrationWeight;

                            Dune::MatrixVector::addToDiagonal(localMatrix[i][j], tIntegral * aIntegral * kernelValue);
                        } // end of ansatz functions loop
                    } // end of test functions loop

                } // end of ansatz quadrature point loop
            } // end of test quadrature point loop
        }


    protected:
        KernelFunction kernel_;
        const int tQuadOrder_;
        const int aQuadOrder_;
};


#endif

