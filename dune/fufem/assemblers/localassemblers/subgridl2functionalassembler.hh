// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUBGRID_L2_FUNCTIONAL_ASSEMBLER_HH
#define SUBGRID_L2_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/gridfunctionhelper.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>

/**  \brief Local assembler for finite element L^2-functionals on the Subgrid, given by Hostgrid-functions
  *
  *  This is needed, e.g. when assembling the right hand side of the spatial problem of a time-discretized time dependent problem with spatial adaptivity.
  *  The solution of the old time step lives on the hostgrid while the rhs is assembled on the NEW subgrid.
  */
template <class GridType, class TestLocalFE, class T=Dune::FieldVector<double,1> >
class SubgridL2FunctionalAssembler :
    public L2FunctionalAssembler<GridType, TestLocalFE, T>

{
    private:
        static const int dim = GridType::dimension;

        typedef typename GridType::HostGridType HostGrid;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        using LocalHostFunction = Dune::Fufem::Impl::LocalFunctionInterfaceForGrid<HostGrid, T>;

    public:
        typedef typename LocalFunctionalAssembler<GridType,TestLocalFE, T>::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename LocalFunctionalAssembler<GridType,TestLocalFE, T>::LocalVector LocalVector;

        typedef typename GridType::HostGridType::template Codim<0>::Entity::Entity HostElement;
        typedef typename GridType::HostGridType::template Codim<0>::Entity::Geometry HostGeometry;

        /** \brief constructor
         *
         * Creates a local functional assembler for an L2-functional.
         * It can assemble functionals on the subgrid given by grid
         * functions on the underlying hostgrid exactly.
         *
         * The QuadratureRuleKey given here does specify what is
         * needed to integrate f.
         *
         * \param f the (hostgrid) function representing the functional
         * \param grid the subgrid (!)
         * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
         */
        template<class FT>
        SubgridL2FunctionalAssembler(const FT& f, const GridType& grid, const QuadratureRuleKey& fQuadKey) :
            L2FunctionalAssembler<GridType,TestLocalFE, T>(f, fQuadKey),
            grid_(grid),
            localHostF_(Dune::Fufem::Impl::localFunctionForGrid<HostGrid, T>(f)),
            functionQuadKey_(fQuadKey)
        {}

        /** \copydoc L2FunctionalAssembler::assemble
         */
        void assemble(const Element& element, LocalVector& localVector, const TestLocalFE& tFE) const
        {
            typedef typename TestLocalFE::Traits::LocalBasisType::Traits::RangeType LocalBasisRangeType;

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            const auto baseHostElement = grid_.template getHostEntity<0>(element);

            // store values of shape functions
            std::vector<LocalBasisRangeType> values(tFE.localBasis().size());

            if (baseHostElement.isLeaf())
            {
                // get quadrature rule
                QuadratureRuleKey quadKey(tFE);
                quadKey.setGeometryType(element.type());
                quadKey = quadKey.product(functionQuadKey_);
                const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

                localHostF_.bind(baseHostElement);

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const LocalCoordinate& quadPos = quad[pt].position();

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // evaluate basis functions
                    tFE.localBasis().evaluateFunction(quadPos, values);

                    // compute values of function
                    auto f_pos = localHostF_(quadPos);

                    // and vector entries
                    for (size_t i=0; i<values.size(); ++i)
                    {
                        localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                    }
                }

            }
            else // corresponding hostgrid element is not in hostgrid leaf
            {
                for (const auto& hostElement : descendantElements(baseHostElement, grid_.getHostGrid().maxLevel()))
                {
                    if (hostElement.isLeaf())
                    {
                        const HostGeometry hostGeometry = hostElement.geometry();

                        // get quadrature rule
                        QuadratureRuleKey quadKey(tFE);
                        quadKey.setGeometryType(hostElement.type());
                        quadKey = quadKey.product(functionQuadKey_);
                        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

                        localHostF_.bind(hostElement);

                        // loop over quadrature points
                        for (size_t pt=0; pt < quad.size(); ++pt)
                        {
                            // get quadrature point
                            const LocalCoordinate& quadPos = quad[pt].position();
                            const LocalCoordinate quadPosInSubgridElement = geometry.local(hostGeometry.global(quadPos)) ;

                            // get integration factor
                            const double integrationElement = hostGeometry.integrationElement(quadPos);

                            // evaluate basis functions
                            tFE.localBasis().evaluateFunction(quadPosInSubgridElement, values);

                            // compute values of function
                            auto f_pos = localHostF_(quadPos);


                            // and vector entries
                            for (size_t i=0; i<values.size(); ++i)
                            {
                                localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                            }
                        }
                    }
                }
            }

            return;
        }

    private:
        const GridType& grid_;
        mutable LocalHostFunction localHostF_;
        const QuadratureRuleKey functionQuadKey_;
};

#endif

