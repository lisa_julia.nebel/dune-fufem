// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH


#include <dune/grid/common/capabilities.hh>

#include <dune/fufem/parallel/parallelalgorithm.hh>



namespace Dune {
namespace Fufem {


//! Generic global assembler for functionals on a gridview
template <class TestBasis>
class DuneFunctionsFunctionalAssembler
{
  using GridView = typename TestBasis::GridView;

public:
  //! create assembler for grid
  DuneFunctionsFunctionalAssembler(const TestBasis& tBasis) :
      testBasis_(tBasis)
  {}



  template <class VectorBackend, class LocalAssembler>
  void assembleBulkEntries(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler) const
  {
    auto testLocalView     = testBasis_.localView();

    using Field = std::decay_t<decltype(vectorBackend[testLocalView.index(0)])>;
    using LocalVector = Dune::BlockVector<Dune::FieldVector<Field,1>>;

    auto localVector = LocalVector(testLocalView.maxSize());

    for (const auto& element : elements(testBasis_.gridView()))
    {
      testLocalView.bind(element);

      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        localVector[localRow] = 0;
      }

      localAssembler(element, localVector, testLocalView);

      // Add element stiffness matrix onto the global stiffness matrix
      for (size_t i=0; i<testLocalView.tree().size(); ++i)
      {
        auto localRow = testLocalView.tree().localIndex(i);
        auto row = testLocalView.index(localRow);
        vectorBackend[row] += localVector[localRow];
      }
    }
  }

  template <class VectorBackend, class LocalAssembler, class ElementPartition>
  void assembleBulkEntries(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler, const ElementPartition& elementPartition, std::size_t threadCount) const
  {
    static_assert(Dune::Capabilities::viewThreadSafe<typename GridView::Grid>::v, "Trying to use thread parallel assembler but grid is not viewThreadSafe.");

    parallelAlgorithm(threadCount, [&, localAssembler](auto parallel) mutable {
      auto testLocalView = testBasis_.localView();

      using Field = std::decay_t<decltype(vectorBackend[testLocalView.index(0)])>;
      using LocalVector = Dune::BlockVector<Dune::FieldVector<Field,1>>;

      auto localVector = LocalVector(testLocalView.maxSize());

      parallel.coloredForEach(elementPartition, [&](const auto& element) {
        testLocalView.bind(element);

        for (size_t i=0; i<testLocalView.tree().size(); ++i)
        {
          auto localRow = testLocalView.tree().localIndex(i);
          localVector[localRow] = 0;
        }

        localAssembler(element, localVector, testLocalView);

        // Add element stiffness matrix onto the global stiffness matrix
        for (size_t i=0; i<testLocalView.tree().size(); ++i)
        {
          auto localRow = testLocalView.tree().localIndex(i);
          auto row = testLocalView.index(localRow);
          vectorBackend[row] += localVector[localRow];
        }
      });
    });
  }


  template <class VectorBackend, class LocalAssembler>
  void assembleBulk(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler) const
  {
    vectorBackend.resize(testBasis_);
    vectorBackend.vector() = 0.0;
    assembleBulkEntries(vectorBackend, std::forward<LocalAssembler>(localAssembler));
  }

  template <class VectorBackend, class LocalAssembler, class ElementPartition>
  void assembleBulk(VectorBackend&& vectorBackend, LocalAssembler&& localAssembler, const ElementPartition& elementPartition, std::size_t threadCount) const
  {
    vectorBackend.resize(testBasis_);
    vectorBackend.vector() = 0.0;
    assembleBulkEntries(vectorBackend, std::forward<LocalAssembler>(localAssembler), elementPartition, threadCount);
  }


protected:
  const TestBasis& testBasis_;
};


/**
 * \brief Create DuneFunctionsFunctionalAssembler
 */
template <class TestBasis>
auto duneFunctionsFunctionalAssembler(const TestBasis& testBasis)
{
  return DuneFunctionsFunctionalAssembler<TestBasis>(testBasis);
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_ASSEMBERS_DUNEFUNCTIONSFUNCTIONALASSEMBLER_HH

