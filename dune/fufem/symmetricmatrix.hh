#ifndef DUNE_FUFEM_SYMMETRICMATRIX_HH
#define DUNE_FUFEM_SYMMETRICMATRIX_HH

#include <dune/common/fvector.hh>

namespace Dune {
namespace Fufem {

/** \brief  A class implementing a symmetric matrix with compile-time size
 *
 *  A \f$ dim\times dim \f$ matrix is stored internally as a <tt>Dune::FieldVector<double, dim*(dim+1)/2></tt>
 *  The components are assumed to be \f$ [ E(1,1),\  E(2,1),\ E(2,2),\ E(3,1),\ E(3,2),\ E(3,3) ]\f$
 *  and analogous for other dimensions
 *  \tparam  dim number of lines/columns of the matrix
 */
template <class T, int N>
class SymmetricMatrix
{
public:

  /** \brief The type used for scalars
   */
  using field_type = T;

  using Data = Dune::FieldVector<T,N*(N+1)/2>;

  /** \brief Default constructor, creates uninitialized matrix
   */
  SymmetricMatrix()
  {}


  /** \brief Construct from raw memory */
  template <class Iterator>
  SymmetricMatrix(Iterator it)
  {
    for (size_t i=0; i<data_.size(); ++i, ++it)
      data_[i] = *it;
  }

  /** \brief Construct identityMatrix */
  static constexpr SymmetricMatrix<T,N> identityMatrix()
  {
    SymmetricMatrix<T,N> id;
    for( size_t i=0; i<N; ++i)
      id.setEntry(i,i,1);

    return id;
  }

    /** \brief return the trace map in vector representation
     *  the returned vector is such that
     *  <data,a> = trace(A) where A.data = a
     */
  static constexpr Data traceMap()
  {
    // trace(A) = <Id,A>, therefore return data of Id
    // use isometry!
    return identityMatrix().data();
  }

  SymmetricMatrix<T,N>& operator=(const T& s)
  {
    data_ = s;
    return *this;
  }

  SymmetricMatrix<T,N>& operator*=(const T& s)
  {
    data_ *= s;
    return *this;
  }

  /** \brief Random write access to components
   *  \param i line index
   *  \param j column index
   *  The off-diagonal entries are scaled by sqrt(2) to ensure isometry
   *  \note: You need to know what you are doing: Modifying an off-diagonal
   *         entry modifies two entries in the matrix!
   */
  void setEntry(int i, int j, const T& entry)
  {
    if( i > j )
      data_[i*(i+1)/2+j] = std::sqrt(2.) * entry;
    else if ( i < j )
      data_[j*(j+1)/2+i] = std::sqrt(2.) * entry;
    else // ( i == j )
      data_[i*(i+1)/2+i] = entry;
  }

  /** \brief Matrix style random read access to components
   *  \param i line index
   *  \param j column index
   *  The off-diagonal entries are scaled by 1/sqrt(2) to ensure isometry
   */
  T operator() (int i, int j) const
  {
    if( i > j )
      return 1./std::sqrt(2.) * data_[i*(i+1)/2+j];
    else if ( i < j )
      return 1./std::sqrt(2.) * data_[j*(j+1)/2+i];
    else // ( i == j )
      return data_[i*(i+1)/2+i];
  }

  T energyScalarProduct (const FieldVector<T,N>& v1, const FieldVector<T,N>& v2) const
  {
    T result = 0;
    for (size_t i=0; i<N; i++)
      for (size_t j=0; j<=i; j++)
          result += (1-0.5*(i==j)) * operator()(i,j) * (v1[i] * v2[j] + v1[j] * v2[i]);
    return result;
  }

  void axpy(const T& a, const SymmetricMatrix<T,N>& other)
  {
    data_.axpy(a,other.data_);
  }

  /** \brief Compute the Frobenius norm of the matrix
   *
   * We have a guaranteed isometric mapping between the Frobenius norm
   * of the matrix and the two-norm of the data vector!
   */
  T frobenius_norm2() const
  {
    return data_.two_norm2();
  }

  /** \brief Compute the Frobenius norm of the matrix */
  T frobenius_norm() const
  {
    return std::sqrt(frobenius_norm2());
  }

  const Data& data() const
  {
    return data_;
  }

  Data& data()
  {
    return data_;
  }

  /** \brief Number of scalars needed for compressed vector storage of the symmetric matrix
   */
  static constexpr size_t dataSize()
  {
    return Data::size();
  }

private:
  Data data_;
};

} // namespace Fufem
} // namespace Dune

#endif

