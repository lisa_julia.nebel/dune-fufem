#ifndef ARITHMETIC_HH
#define ARITHMETIC_HH

#include <type_traits>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/unused.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/axy.hh>
#include <dune/matrix-vector/crossproduct.hh>
#include <dune/matrix-vector/promote.hh>
#include <dune/matrix-vector/transpose.hh>

/** \brief Namespace containing helper classes and functions for arithmetic operations
 *
 * Everything in this namespace is experimental and might change
 * in the near future. Especially the naming of namespace, structs,
 * and functions is not final.
 */
namespace Arithmetic
{

    // type promotion (smallest matrix that can hold the sum of two matrices *******************
    template<class MatrixA, class MatrixB>
    struct Promote
    {
      typedef typename Dune::MatrixVector::Promote<MatrixA, MatrixB>::Type Type;
    };

    /** \brief Class to identify scalar types
     *
     * Specialize this class for all types that can be used
     * like scalar quantities.
     */
    template<class T>
    struct ScalarTraits
    {
        enum { isScalar=(std::is_scalar<T>::value and not std::is_pointer<T>::value)};
    };

    template<class T>
    struct ScalarTraits<Dune::FieldVector<T,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::FieldMatrix<T,1,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::DiagonalMatrix<T,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::ScaledIdentityMatrix<T,1> >
    {
        enum { isScalar=true };
    };


    /** \brief Class to identify matrix types and extract information
     *
     * Specialize this class for all types that can be used like a matrix.
     */
    template<class T>
    struct MatrixTraits
    {
        enum { isMatrix=false };
        enum { rows=-1};
        enum { cols=-1};
    };

    template<class T, int n, int m>
    struct MatrixTraits<Dune::FieldMatrix<T,n,m> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=m};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::DiagonalMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::ScaledIdentityMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T>
    struct MatrixTraits<Dune::BCRSMatrix<T> >
    {
        enum { isMatrix=true };
    };

    template <bool Condition, typename Type=void>
    using enable_if_t = typename std::enable_if<Condition, Type>::type;

    template <class MatrixType>
    using Transposed = typename Dune::MatrixVector::Transposed<MatrixType>;

    /** \brief Add a product to some matrix or vector
     *
     * This function computes a+=b*c.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,b,c could be matrices with appropriate
     * dimensions. But b can also always be a scalar
     * represented by a 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C>
    void addProduct(A& a, const B& b, const C& c)
    {
      Dune::MatrixVector::addProduct(a,b,c);
    }

    /** \brief Subtract a product from some matrix or vector
     *
     * This function computes a-=b*c.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,b,c could be matrices with appropriate
     * dimensions. But b can also always be a scalar
     * represented by a 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C>
    void subtractProduct(A& a, const B& b, const C& c)
    {
      Dune::MatrixVector::subtractProduct(a,b,c);
    }

    //! Compute the cross product of two vectors. Only works for n==3
    template<class T, int n>
    Dune::FieldVector<T,n> crossProduct(const Dune::FieldVector<T,n>& a, const Dune::FieldVector<T,n>& b)
    {
      return Dune::MatrixVector::crossProduct(a,b);
    }

    //! Compute the transposed of a matrix
    template <class A>
    void transpose(const A& a, Transposed<A>& aT)
    {
      Dune::MatrixVector::transpose(a,aT);
    }

    /** \brief Add a scaled product to some matrix or vector
     *
     * This function computes a+=b*c*d.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,c,d could be matrices with appropriate dimensions. But b must
     * (currently) and c can also always be a scalar represented by a
     * 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C, class D>
    typename std::enable_if<ScalarTraits<B>::isScalar, void>::type
    addProduct(A& a, const B& b, const C& c, const D& d)
    {
      Dune::MatrixVector::addProduct(a,b,c,d);
    }

    /** \brief Subtract a scaled product from some matrix or vector
     *
     * This function computes a-=b*c*d.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,c,d could be matrices with appropriate dimensions. But b must
     * (currently) and c can also always be a scalar represented by a
     * 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C, class D>
    typename std::enable_if<ScalarTraits<B>::isScalar, void>::type
    subtractProduct(A& a, const B& b, const C& c, const D& d)
    {
      Dune::MatrixVector::subtractProduct(a,b,c,d);
    }

    //! Compute \f$(Ax,y)\f$
    template <class OperatorType, class VectorType, class VectorType2>
    typename VectorType::field_type
    Axy(const OperatorType &A,
        const VectorType &x,
        const VectorType2 &y)
    {
        return Dune::MatrixVector::Axy(A, x, y);
    }

    //! Compute \f$(b-Ax,y)\f$
    template <class OperatorType, class VectorType, class VectorType2>
    typename VectorType::field_type
    bmAxy(const OperatorType &A,
          const VectorType2 &b,
          const VectorType &x,
          const VectorType2 &y)
    {
        return Dune::MatrixVector::bmAxy(A, b, x, y);
    }
}

#endif
