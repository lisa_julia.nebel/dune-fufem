#ifndef ELASTICITYTENSOR_HH
#define ELASTICITYTENSOR_HH

#include <dune/common/fmatrix.hh>

/** \brief Base class for elasticity tensors in Voigt notation
 */
template <int dim>
class ElasticityTensor : public Dune::FieldMatrix<double, (dim+1)*dim/2, (dim+1)*dim/2>
{
    public:
        /** \brief Default constructor */
        ElasticityTensor() {}

        /**  */
        ElasticityTensor(const double a): Dune::FieldMatrix<double, (dim+1)*dim/2, (dim+1)*dim/2>(a) {}

        using Dune::FieldMatrix<double, (dim+1)*dim/2, (dim+1)*dim/2>::operator=;
};

#endif
