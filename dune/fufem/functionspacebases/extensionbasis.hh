#ifndef EXTENSIONBASIS_HH
#define EXTENSIONBASIS_HH

#include <dune/common/bitsetvector.hh>


#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p2nodalbasis.hh>
#include <dune/fufem/functionspacebases/p2hierarchicalbasis.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>


// forward declaration
template <class Basis, class ExtendedBasis> struct SpaceExtensionSetup;


/** \brief This class describes the extension of a space given by a basis in an extended space given by a larger basis
 *
 */
template <class Basis, class ExtendedBasis>
class ExtensionBasis :
    public FunctionSpaceBasis<
        typename ExtendedBasis::GridView,
        typename ExtendedBasis::ReturnType,
        typename ExtendedBasis::LocalFiniteElement>

{
    protected:
        typedef typename ExtendedBasis::GridView GV;
        typedef typename ExtendedBasis::ReturnType RT;
        typedef typename ExtendedBasis::LocalFiniteElement LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::BitVector BitVector;
        typedef typename Base::LinearCombination LinearCombination;


        ExtensionBasis(const Basis& basis, const ExtendedBasis& extendedBasis) :
            Base(extendedBasis.getGridView()),
            basis_(basis),
            extendedBasis_(extendedBasis)
        {
            update();
        }

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            update();
        }

        size_t size() const
        {
            return extendedBasis_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return extendedBasis_.getLocalFiniteElement(e);
        }

        int index(const Element& e, const int i) const
        {
            return extendedBasis_.index(e, i);
        }

        bool isConstrained(const int index) const
        {
            return dofConstraints_.isConstrained()[index][0];
        }

        const LinearCombination& constraints(const int index) const
        {
            return dofConstraints_.interpolation()[index];
        }

        const BitVector& isConstrained() const
        {
            return dofConstraints_.isConstrained();
        }

        BitVector& isExtensionDOF()
        {
            return isExtensionDOF_;
        }

        const BitVector& isExtensionDOF() const
        {
            return isExtensionDOF_;
        }

    protected:
        void update()
        {
            SpaceExtensionSetup<Basis, ExtendedBasis>::setup(basis_, extendedBasis_, *this);

            dofConstraints_.resize(extendedBasis_.size());
            for (size_t i=0; i<extendedBasis_.size(); ++i)
            {
                if (isExtensionDOF_[i][0])
                    dofConstraints_.isConstrained()[i][0] = false;
                else
                {
                    dofConstraints_.isConstrained()[i][0] = true;
                    if (extendedBasis_.isConstrained(i))
                    {
                        const LinearCombination& constraints = extendedBasis_.constraints(i);
                        for (size_t j=0; j<constraints.size(); ++j)
                        {
                            if (isExtensionDOF_[constraints[j].index][0])
                                dofConstraints_.interpolation()[i].push_back(constraints[j]);
                        }
                    }
                }
            }
        }

    private:
        const Basis& basis_;
        const ExtendedBasis& extendedBasis_;

        BitVector isExtensionDOF_;

        DOFConstraints dofConstraints_;
};



// default implementation
template <class Basis, class ExtendedBasis>
struct SpaceExtensionSetup
{
    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        DUNE_THROW(Dune::NotImplemented, "Setup for this combination of basis and extended bases not implemented yet!");
    }
};



// default implementation
template <class Basis, class ExtendedBasis>
struct ExtensionFirstSpaceExtensionSetup
{
    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        extension.isExtensionDOF().resize(extendedBasis.size());
        size_t extensionSize = extendedBasis.size() - basis.size();
        for (size_t i=0; i<extendedBasis.size(); ++i)
            extension.isExtensionDOF()[i] = ((i<extensionSize) and not(extendedBasis.isConstrained(i)));
    }
};



// specialization for P1-P2
template <class GV, class RT>
struct SpaceExtensionSetup<
    P1NodalBasis<GV, RT>,
    P2NodalBasis<GV, RT> >
{
    typedef P1NodalBasis<GV, RT> Basis;
    typedef P2NodalBasis<GV, RT> ExtendedBasis;

    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        ExtensionFirstSpaceExtensionSetup<Basis,ExtendedBasis>::setup(basis, extendedBasis, extension);
    }
};



// specialization for P1-HierarchicalP2
template <class GV, class RT>
struct SpaceExtensionSetup<
    P1NodalBasis<GV, RT>,
    P2HierarchicalBasis<GV, RT> >
{
    typedef P1NodalBasis<GV, RT> Basis;
    typedef P2HierarchicalBasis<GV, RT> ExtendedBasis;

    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        ExtensionFirstSpaceExtensionSetup<Basis,ExtendedBasis>::setup(basis, extendedBasis, extension);
    }
};



// specialization for P1-RefinedP1
template <class GV, class RT>
struct SpaceExtensionSetup<
    P1NodalBasis<GV, RT>,
    RefinedP1NodalBasis<GV, RT> >
{
    typedef P1NodalBasis<GV, RT> Basis;
    typedef RefinedP1NodalBasis<GV, RT> ExtendedBasis;

    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        ExtensionFirstSpaceExtensionSetup<Basis,ExtendedBasis>::setup(basis, extendedBasis, extension);
    }
};



// specialization for ConformingP1-ConformingRefinedP1
template <class GV, class RT>
struct SpaceExtensionSetup<
    ConformingBasis< P1NodalBasis<GV, RT> >,
    ConformingBasis< RefinedP1NodalBasis<GV, RT> > >
{
    typedef ConformingBasis< P1NodalBasis<GV, RT> > Basis;
    typedef ConformingBasis< RefinedP1NodalBasis<GV, RT> > ExtendedBasis;

    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        ExtensionFirstSpaceExtensionSetup<Basis,ExtendedBasis>::setup(basis, extendedBasis, extension);
    }
};



// specialization for ConformingP1-ConformingP2
template <class GV, class RT>
struct SpaceExtensionSetup<
    ConformingBasis< P1NodalBasis<GV, RT> >,
    ConformingBasis< P2NodalBasis<GV, RT> > >
{
    typedef ConformingBasis< P1NodalBasis<GV, RT> > Basis;
    typedef ConformingBasis< P2NodalBasis<GV, RT> > ExtendedBasis;

    template <class EB>
    static void setup(const Basis& basis, const ExtendedBasis& extendedBasis, EB& extension)
    {
        ExtensionFirstSpaceExtensionSetup<Basis,ExtendedBasis>::setup(basis, extendedBasis, extension);
    }
};

#endif

