#ifndef DOF_CONSTRAINTS_HH
#define DOF_CONSTRAINTS_HH

#include <cmath>
#include <vector>
#include <map>

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/fufem/functions/cachedfunction.hh>

/** \brief Class to handle linear constraints on a subset of a set of DOFs
 *
 * This class handles linear constraints on a subset of DOFs of the form
 *
 * \f[ x_i = \sum_{I} \lambda_j x_j \f]
 *
 * so it essentially stores a set of coefficients for linear combinations.
 */
class DOFConstraints {
    public:

        class LinearFactor
        {
            public:
                LinearFactor() : index(0), factor(1.0)
                {}

                LinearFactor(int index, double factor) : index(index), factor(factor)
                {}

                int index;
                double factor;
        };

        typedef std::vector <LinearFactor> LinearCombination;
        typedef std::vector< LinearCombination > Interpolation;


        //! create hanging node set
        DOFConstraints()
        {}


        /*
         *  \todo please doc me
         */
        DOFConstraints(int size)
        {
            resize(size);
        }


        /*
         *  \todo please doc me
         */
        void clear()
        {
            isConstrained_.clear();
            interpolation_.clear();
            resize(0);
        }


        /*
         *  \todo please doc me
         */
        void resize(int size)
        {
            isConstrained_.resize(size);
            isConstrained_.unsetAll();

            LinearCombination empty;
            interpolation_.resize(size);
            interpolation_.assign(interpolation_.size(), empty);
        }


        /*
         *  \todo please doc me
         */
        template<class B>
        void setupFromBasis(const B& basis)
        {
            typedef typename B::GridView::template Codim<0>::Entity Element;
            using LocalCoordinate = typename Element::Geometry::LocalCoordinate;

            typedef typename B::LocalFiniteElement FE;

            using RangeType = typename FE::Traits::LocalBasisType::Traits::RangeType;

            typedef std::vector< std::map<int, double> > InterpolationMap;

            const typename B::GridView& gridview = basis.getGridView();

            resize(basis.size());

            InterpolationMap interpolationMap(basis.size());

            for(const auto& element : elements(gridview))
            {
                int level = element.level();
                const auto& localBasis = basis.getLocalFiniteElement(element).localBasis();
                const auto& localInterpolation = basis.getLocalFiniteElement(element).localInterpolation();

                // check if all dofs on element are already constrained
                bool allDofsConstrained = true;
                for(size_t i=0; i<localBasis.size(); ++i)
                    allDofsConstrained = allDofsConstrained and isConstrained_[basis.index(element, i)][0];

                if (allDofsConstrained)
                    continue;

                for(const auto& intersection : intersections(gridview, element))
                {
                    if (not(intersection.neighbor()))
                        continue;

                    const Element& neighbor = intersection.outside();

                    if (not(neighbor.level()<level))
                        continue;

                    const auto& nLocalBasis = basis.getLocalFiniteElement(neighbor).localBasis();

                    auto neighborBasis_all_cached = Dune::Fufem::CachedFunction([&](auto&&x) {
                        std::vector<RangeType> yy;
                        auto globalPosition = element.geometry().global(x);
                        auto neigborLocalPosition = neighbor.geometry().local(globalPosition);
                        auto&& neigborRefElement = Dune::referenceElement<double, Element::dimension>(neighbor.type());
                        if (neigborRefElement.checkInside(neigborLocalPosition))
                            nLocalBasis.evaluateFunction(neigborLocalPosition, yy);
                        else
                        {
                            yy.resize(nLocalBasis.size());
                            for(size_t j=0; j<nLocalBasis.size(); ++j)
                                yy[j] = 0;
                        }
                        return yy;
                    }, Dune::MetaType<LocalCoordinate>());


                    std::vector<double> interpolationValues(localBasis.size());

                    Dune::BitSetVector<1> localDofIsConstrained(localBasis.size(), false);

                    for(size_t j=0; j<nLocalBasis.size(); ++j)
                    {
                        int jIndex = basis.index(neighbor, j);

                        auto neighborBasis_j = [&](auto&&x) { return neighborBasis_all_cached(x)[j]; };
                        localInterpolation.interpolate(neighborBasis_j, interpolationValues);

                        for(size_t i=0; i<localBasis.size(); ++i)
                        {
                            int iIndex = basis.index(element, i);

                            // we need to skip this if dof is already constrained from another element
                            // i.e. isConstrained_ is already set
                            // otherwise we might get interpolation weights from multiple level neighbors
                            // resulting in doubled interpolation weights after resolving the dependencies
                            if (isConstrained_[iIndex][0])
                                continue;

                            if (iIndex != jIndex)
                            {
                                if (std::abs(interpolationValues[i]) > 1e-5)
                                {
                                    interpolationMap[iIndex][jIndex] = interpolationValues[i];
                                    localDofIsConstrained[i][0] = true;
                                }
                            }
                        }
                    }
                    for(size_t i=0; i<localBasis.size(); ++i)
                    {
                        if (localDofIsConstrained[i][0])
                            isConstrained_[basis.index(element, i)] = true;
                    }
                }
            }

            // copy linear combinations
            for(size_t i=0; i<interpolationMap.size(); ++i)
            {
                for(const auto& it : interpolationMap[i])
                    interpolation_[i].push_back(LinearFactor(it.first,it.second));
            }

            resolveDependencies();
#ifndef NDEBUG
            check();
#endif
        }


        /*
         *  \todo please doc me
         */
        const Dune::BitSetVector<1>& isConstrained() const
        {
            return isConstrained_;
        }


        /*
         *  \todo please doc me
         */
        Dune::BitSetVector<1>& isConstrained()
        {
            return isConstrained_;
        }


        /*
         *  \todo please doc me
         */
        const Interpolation& interpolation() const
        {
            return interpolation_;
        }


        /*
         *  \todo please doc me
         */
        Interpolation& interpolation()
        {
            return interpolation_;
        }


        /**
         * \brief Report the constraints to std::cout.
         *
         */
        void report() const
        {
            int size = isConstrained_.size();
            int count = 0.0;
            for (int i=0; i<size; ++i)
            {
                if (isConstrained_[i][0])
                {
                    ++count;
                    report(i);
                }
            }
            std::cout << "interpolated dofs : " << count << " / " << isConstrained_.size() << std::endl;
        }

        void report(int dofIndex) const
        {
            std::cout << "x_" << dofIndex << " = ";

            for(const auto& it : interpolation_[dofIndex])
                std::cout << it.factor << " * x_" << it.index << " + " ;
            std::cout << std::endl;
        }

        /**
         * \brief Check constraints for consistency.
         *
         */
        bool check() const
        {
            bool r = true;

            int size = isConstrained_.size();
            for (int i=0; i<size; ++i)
            {
                if (isConstrained_[i][0])
                {
                    double sum = 0.0;
                    bool errorForDof = false;

                    for(const auto& it : interpolation_[i])
                    {
                        sum += it.factor;
                        if (isConstrained_[it.index][0])
                        {
                            std::cout << "Error: interpolation for constrained DOF " << i << " depends on constrained DOF " << it.index << std::endl;
                            errorForDof = true;
                        }
                    }

                    if (std::abs(sum-1.0)>1e-10)
                    {
                        std::cout << "Error: interpolation values for constrained DOF " << i << " sum up to " << sum << " != 1" << std::endl;
                        errorForDof = true;
                    }
                    r = r and not(errorForDof);
                    if (errorForDof)
                        report(i);
                }
            }
            return r;
        }


    protected:

        /**
         * \brief Resolve dependencies of constrained DOFs on other constrained DOFs.
         *
         */
        void resolveDependencies()
        {
            int size = isConstrained_.size();
            Dune::BitSetVector<1> dependenciesResolved(size, false);

            for (int i=0; i<size; ++i)
                if (not(dependenciesResolved[i][0]))
                    resolveDOF(i, dependenciesResolved);
        }


        /**
         * \brief Recursively resolve dependencies of specific constrained DOF and all dependent DOFs.
         *
         */
        void resolveDOF(int i, Dune::BitSetVector<1>& dependenciesResolved)
        {
            if (isConstrained_[i][0] and not(dependenciesResolved[i][0]))
            {
                LinearCombination inserted;

                auto it = interpolation_[i].begin();
                while (it != interpolation_[i].end())
                {
                    // only factors which are hanging itself have to be inserted recursively
                    if (isConstrained_[it->index][0])
                    {
                        // if the hanging node factor itself is not resolved resolve it first
                        if (not(dependenciesResolved[it->index][0]))
                            resolveDOF(it->index, dependenciesResolved);

                        // now insert the factors of the constrained DOFs
                        // these dofs of the inserted are all nonhanging itself, since they have 
                        // already been resolved by the recursive resolveDOF above
                        for (const auto& depIt : interpolation_[it->index])
                            inserted.push_back(LinearFactor(depIt.index, it->factor * depIt.factor));

                        it = interpolation_[i].erase(it);
                    }
                    else
                        ++it;
                }
                interpolation_[i].insert(interpolation_[i].end(), inserted.begin(), inserted.end());
            }
            dependenciesResolved[i][0] = true;
        }


        Dune::BitSetVector<1> isConstrained_;
        Interpolation interpolation_;
};

#endif
