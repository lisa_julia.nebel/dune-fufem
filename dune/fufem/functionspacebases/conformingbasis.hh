#ifndef CONFORMING_BASIS_HH
#define CONFORMING_BASIS_HH

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/functionspacebases/dofconstraints.hh"


/** \brief Generic class for conforming global basis
 *
 * This class provides a generic global basis of a conforming (C^0)
 * finite element space. The space is constructed from a basis of
 * a nonconforming space using an internal DOFConstraints. You can
 * also provide the latter on your own using the appropriate constructor.
 *
 * \tparam NonconformingBasis The underlying nonconforming basis.
 */
template <class NonconformingBasis>
class ConformingBasis :
    public FunctionSpaceBasis<
        typename NonconformingBasis::GridView,
        typename NonconformingBasis::ReturnType,
        typename NonconformingBasis::LocalFiniteElement>

{
    protected:
        typedef typename NonconformingBasis::GridView GV;
        typedef typename NonconformingBasis::ReturnType RT;
        typedef typename NonconformingBasis::LocalFiniteElement LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::BitVector BitVector;
        typedef typename Base::LinearCombination LinearCombination;

        /**
         * \brief Setup conforming global basis
         *
         * This constructor will take some time since the interpolation values
         * for the conforming basis need to be build.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         */
        ConformingBasis(const NonconformingBasis& ncBasis) :
            Base(ncBasis.getGridView()),
            ncBasis_(ncBasis)
        {
            dofConstraints_ = new DOFConstraints;
            dofConstraintsAllocated_ = true;
            dofConstraints_->setupFromBasis<NonconformingBasis>(ncBasis_);
        }

        /**
         * \brief Setup conforming global basis
         *
         * Using this constructor you can provide a custom DOFConstrains object.
         * This needs to be set up before using the basis and after grid modification manually.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         * \param dofConstraints Set of interpolation values for the conforming subspace basis
         */
        ConformingBasis(const NonconformingBasis& ncBasis, const DOFConstraints& dofConstraints) :
            Base(ncBasis.getGridView()),
            ncBasis_(ncBasis),
            dofConstraints_(const_cast<DOFConstraints*>(&dofConstraints)),
            dofConstraintsAllocated_(false)
        {}

        /**
         * \brief Deep copy
         */
        ConformingBasis(const ConformingBasis& from) :
            Base(from.getGridView()),
            ncBasis_(from.ncBasis_)
        {
            dofConstraintsAllocated_ = from.dofConstraintsAllocated_;
            if (dofConstraintsAllocated_)
                dofConstraints_ = new DOFConstraints(*(from.dofConstraints_));
            else
                dofConstraints_ = from.dofConstraints_;
        }

        virtual ~ConformingBasis()
        {
            if (dofConstraintsAllocated_)
                delete dofConstraints_;
        }

        using Base::update;

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            if (dofConstraintsAllocated_)
                dofConstraints_->setupFromBasis<NonconformingBasis>(ncBasis_);
        }

        size_t size() const
        {
            return ncBasis_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return ncBasis_.getLocalFiniteElement(e);
        }

        int index(const Element& e, const int i) const
        {
            return ncBasis_.index(e, i);
        }

        bool isConstrained(const int index) const
        {
            return dofConstraints_->isConstrained()[index][0];
        }

        const LinearCombination& constraints(const int index) const
        {
            return dofConstraints_->interpolation()[index];
        }

        const BitVector& isConstrained() const
        {
            return dofConstraints_->isConstrained();
        }

    protected:
        const NonconformingBasis& ncBasis_;
        DOFConstraints* dofConstraints_;
        bool dofConstraintsAllocated_;
};

#endif

