#ifndef DUNE_FUFEM_MAKE_HALF_CIRCLE_HH
#define DUNE_FUFEM_MAKE_HALF_CIRCLE_HH

#include <cmath>
#include <memory>
#include <vector>

#include <dune/common/shared_ptr.hh>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/boundarysegment.hh>

#include "arcofcircle.hh"

template<class GridType>
std::unique_ptr<GridType> createCircle(const Dune::FieldVector<double,2>& center, double r)
{
    static_assert(GridType::dimension==2, "createCircle can only be instantiated for 2d grids");
    
    Dune::GridFactory<GridType> factory;

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////
    double x = std::sqrt((r*r)/2.0);

    factory.insertVertex({center[0]-x, center[1]-x});
    factory.insertVertex({center[0]+x, center[1]-x});
    factory.insertVertex({center[0]-x, center[1]+x});
    factory.insertVertex({center[0]+x, center[1]+x});

    factory.insertVertex(center);


    // /////////////////
    // Insert elements
    // /////////////////

    factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 0, 1});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 1, 3});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 3, 2});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {4, 2, 0});


    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    factory.insertBoundarySegment({0, 1}, SegmentPointer(new ArcOfCircle(center, r, M_PI*5/4, M_PI*7/4)));
    factory.insertBoundarySegment({1, 3}, SegmentPointer(new ArcOfCircle(center, r, M_PI*7/4, M_PI*9/4)));
    factory.insertBoundarySegment({3, 2}, SegmentPointer(new ArcOfCircle(center, r, M_PI*1/4, M_PI*3/4)));
    factory.insertBoundarySegment({2, 0}, SegmentPointer(new ArcOfCircle(center, r, M_PI*3/4, M_PI*5/4)));


    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    return factory.createGrid();
}



template <class GridType>
void makeHalfCircle(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircle can only be instantiated for 2d grids");
    
    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////

    Dune::FieldVector<double,2> center = {0,15};

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    factory.insertBoundarySegment({1, 2}, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));
    factory.insertBoundarySegment({2, 3}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));
    factory.insertBoundarySegment({3, 0}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));


    // ////////////////////////
    //   Insert vertices
    // ////////////////////////

    factory.insertVertex({15,15});
    factory.insertVertex({-15,15});
    factory.insertVertex({-7.5,2.00962});
    factory.insertVertex({7.5,2.00962});

    // /////////////////
    // Insert elements
    // /////////////////

    factory.insertElement(Dune::GeometryTypes::simplex(2), {0,1,2});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {2,3,0});

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();
}

template <class GridType>
void makeHalfCircleQuadTri(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleQuadTri can only be instantiated for 2d grids");
    
    Dune::GridFactory<GridType> factory(&grid);

    // ////////////////////
    //   Create the domain
    // //////////////////////
    Dune::FieldVector<double,2> center = {0,0.4};

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    factory.insertBoundarySegment({1,8}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI, M_PI*9/8)));
    factory.insertBoundarySegment({8,2}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*9/8, M_PI*10/8)));
    factory.insertBoundarySegment({2,4}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*10/8, M_PI*12/8)));
    factory.insertBoundarySegment({4,3}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*12/8, M_PI*14/8)));
    factory.insertBoundarySegment({3,9}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*14/8, M_PI*15/8)));
    factory.insertBoundarySegment({9,0}, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*15/8, M_PI*2)));

    // ////////////////////
    //  Insert inner nodes
    // ////////////////////
    std::vector<Dune::FieldVector<double,2> > nodePos = {{0.4, 0.4},
                             {-0.4, 0.4},
                             {-0.282843, 0.117157},
                             {0.282843, 0.117157},
                             {-7.34788e-17, 0},
                             {0, 0.4},
                             {0.2, 0.4},
                             {-0.2, 0.4},
                             {-0.369552, 0.246927},
                             {0.369552, 0.246927},
                             {0.113395, 0.270427},
                             {0.170737, 0.329338},
                             {-0.170737, 0.329338},
                             {-0.113395, 0.270427},
                             {1.25898e-10, 0.235214}};
    
    for (int i=0; i<15; i++)
        factory.insertVertex(nodePos[i]);

    // /////////////////
    // Insert elements
    // /////////////////

    std::vector<std::vector<unsigned int> > vertices = {{5, 11, 6},
                            {5, 10, 11},
                            {5, 14, 10},
                            {14, 5, 13},
                            {13, 5, 12},
                            {5, 7, 12},
                            {7, 1, 12, 8},
                            {12, 8, 13, 2},
                            {13, 2, 14, 4},
                            {14, 4, 10, 3},
                            {10, 3, 11, 9},
                            {11, 9, 6, 0}};

    for (int i=0; i<6; i++)
        factory.insertElement(Dune::GeometryTypes::simplex(2), vertices[i]);

    for (int i=6; i<12; i++)
        factory.insertElement(Dune::GeometryTypes::cube(2), vertices[i]);
    
    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();
}


template <class GridType>
void makeHalfCircleQuad(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleQuad can only be instantiated for 2d grids");
    
    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    Dune::FieldVector<double,2> center = {0,15};

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    factory.insertBoundarySegment({1,2}, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));
    factory.insertBoundarySegment({2,3}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));
    factory.insertBoundarySegment({3,0}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////

    factory.insertVertex({15,15});
    factory.insertVertex({-15,15});
    factory.insertVertex({-7.5,2.00962});
    factory.insertVertex({7.5,2.00962});

    // /////////////////
    // Insert elements
    // /////////////////

    factory.insertElement(Dune::GeometryTypes::cube(2), {0,1,3,2});

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();
}


template <class GridType>
void makeHalfCircleThreeTris(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleThreeTris can only be instantiated for 2d grids");

    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    Dune::FieldVector<double,2> center = {0,15};

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    factory.insertBoundarySegment({1,2}, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));
    factory.insertBoundarySegment({2,3}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));
    factory.insertBoundarySegment({3,0}, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////

    factory.insertVertex({15,15});
    factory.insertVertex({-15,15});
    factory.insertVertex({-7.5,2.00962});
    factory.insertVertex({7.5,2.00962});
    factory.insertVertex(center);

    // /////////////////
    // Insert elements
    // /////////////////

    factory.insertElement(Dune::GeometryTypes::simplex(2), {1,2,4});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {2,3,4});
    factory.insertElement(Dune::GeometryTypes::simplex(2), {3,0,4});

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();
}

#endif
