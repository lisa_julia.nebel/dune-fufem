// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_PARALLEL_PARALLELALGORITHM_HH
#define DUNE_FUFEM_PARALLEL_PARALLELALGORITHM_HH

#include <cstddef>
#include <vector>
#include <thread>

#include <dune/common/iteratorrange.hh>

#include <dune/fufem/parallel/barrier.hh>



namespace Dune {
namespace Fufem {


/**
 * \brief Split range into uniform segments
 *
 * Given a range, this returns a range-view of the i-th of n contiguous
 * subranges of approximately the same size range.size()/n.
 * To ensure that this is O(1) the passed range should provide
 * random access iterators.
 *
 * \param range The (random access) range that should be split
 * \param i Index of this segment
 * \param n Number of segments
 *
 * \returns A view given by Dune::IteratorRange
 */
template<class Range>
static auto rangeSegment(Range& range, std::size_t i, std::size_t n) {
  std::size_t length = range.size() / n;
  auto begin = length*i;
  auto end = length*(i+1);
  if (i == n-1)
    end = range.size();
  return Dune::IteratorRange(range.begin()+begin, range.begin()+end);
}




namespace Impl {

class ParallelAlgorithmThreadHelper
{
public:
  ParallelAlgorithmThreadHelper(std::size_t threadCount, std::size_t threadIndex, Dune::Fufem::Barrier& barrier):
    threadCount_(threadCount),
    threadIndex_(threadIndex),
    barrier_(barrier)
  {}

  template<class Range, class CallBack>
  void forEach(Range&& range, CallBack&& callBack) const
  {
    for (auto&& entry : rangeSegment(range, threadIndex_, threadCount_))
      callBack(entry);
    barrier_.arrive_and_wait();
  }

  template<class ColorPartition, class CallBack>
  void coloredForEach(const ColorPartition& partition, CallBack&& callBack) const
  {
    for (auto&& singleColorRange : partition)
      forEach(singleColorRange, callBack);
  }

  std::size_t threadIndex() const
  {
    return threadIndex_;
  }

  std::size_t threadCount() const
  {
    return threadCount_;
  }

  Dune::Fufem::Barrier& barrier() const
  {
    return barrier_;;
  }

private:
  std::size_t threadCount_;
  std::size_t threadIndex_;
  Dune::Fufem::Barrier& barrier_;
};

} // namespace Impl



/**
 * \brief Utility for implementing parallel algorithms based on colored entity partitions
 *
 * \param threadCount The number of threads to create
 * \param threadImp The callback implementing the thread
 *
 * This will create threadCount copies of threadImp, one for each thread.
 * For each thread it will execute the respective copy of threadImp passing
 * a callback object parallelAlgorithmHelper to it. This can e.g. be used to
 * do a parallel loop over a range using parallelAlgorithmHelper.forEach(range,f)
 * or parallel loops over each color in a colored partition of a range
 * using parallelAlgorithmHelper.coloredForEach(partition,f).
 * More precisely calling this in a thread will do an outer loop over all colors
 * from the partition.
 * For each color it will execute an inner loop over the range of entries from the
 * current color that have been assigned to the thread and pass them to f.
 * Every time the inner loop has finished all threads will be synchronized
 * with a barrier.
 *
 * This requires that the partition passed to coloredForEach() is overlap-free with respect to f,
 * in the sense that parallel calls of f for different entities of the same color
 * do not create race conditions.
 * Furthermore the additional code in the threads must also not contain race conditions.
 * Then the whole algorithm can safely be executed in parallel.
 * Attention: Remember to always capture data that should be thread-local by value in threadImp.
 * Otherwise concurrent threads will access a single instance of this data.
 *
 * Example: Assume that accessElementData(entity) writes some date associated
 * to the entity and that the partition is overlap-free wrt this function.
 * Then a parallel algorithm can e.g. take the form
 *
 * parallelAlgorithm(threadCount, [&, threadLocalData](auto&& parallel) {
 *   doSomeSetupForThisThread();
 *   parallel.coloredForEach(partition, [&](const auto& entity) {
 *     accessElementData(entity);
 *   });
 *   parallel.barrier().arrive_and_wait();
 *   if (parallel.threadIndex() == 1)
 *     std::cout << "Hello from thread 1!" << std::endl;
 *   parallel.barrier().arrive_and_wait();
 *   doSomeParallelComputation();
 *   parallel.barrier().arrive_and_wait();
 *   doSomeMoreParallelComputation();
 * });
 */
template<class F>
void parallelAlgorithm(std::size_t threadCount, F&& threadImp)
{
  Dune::Fufem::Barrier barrier(threadCount);

  std::vector<std::thread> threads;
  for (std::size_t i=0; i<threadCount; ++i)
    threads.emplace_back(threadImp, Impl::ParallelAlgorithmThreadHelper(threadCount, i, barrier));
  for (auto& thread : threads)
    thread.join();
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_PARALLEL_PARALLELALGORITHM_HH

