// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_PARALLEL_ELEMENTCOLORING_HH
#define DUNE_FUFEM_PARALLEL_ELEMENTCOLORING_HH

#include <cstddef>
#include <type_traits>
#include <utility>
#include <execution>
#include <vector>
#include <unordered_map>
#include <queue>
#include <algorithm>
#include <limits>
#include <iostream>

#include <dune/common/timer.hh>
#include <dune/common/rangeutilities.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/grid/common/scsgmapper.hh>



namespace Dune {
namespace Fufem {



/**
 * \brief Compute a graph coloring from neighboring information
 *
 * This computes a coloring for a set of nodes with given neighboring relation
 * using a simple greedy advancing front algorithm.
 *
 * This overload allows to additionally pass a container of seed nodes.
 * The algorithm is the same as for the overload wohout this argument, but
 * the internal queue is initialized with the given seed nodes instead of 0.
 * This may improve performance if there are many connected components.
 * Passing an empty container leads to the same behaviour as passing none.
 *
 * \param neighbors A callback that maps each node to the range of its neighbors
 * \param size Number of nodes in the graph
 * \param seedNodes Container with seed nodes to be used for advancing front method.
 *
 * \returns A std::vector<size_type> containing the color of each node index
 */
template <class NeighborMap, class SeedNodesContainer>
auto computeColoring(const NeighborMap& neighbors, std::size_t size, SeedNodesContainer&& seedNodes)
{
  using size_type = std::decay_t<decltype(*neighbors(0).begin())>;

  // Use magic values to tag fully unprocessed and queued (but non-colored) entries.
  size_type nonProcessedStatus = std::numeric_limits<size_type>::max();
  size_type queuedStatus = std::numeric_limits<size_type>::max()-1;

  // Initialize result vector with unprocessed state for all entries.
  std::vector<size_type> color(size, nonProcessedStatus);

  // Queue for the advancing front method
  auto queue = std::queue<size_type>();

  // Some helper methods to make code more expressive
  auto isColored = [&](const auto& i) {
    return color[i]<queuedStatus;
  };
  auto isNonProcessed = [&](const auto& i) {
    return color[i]==nonProcessedStatus;
  };
  auto queueNode = [&](const auto& i) {
    color[i] = queuedStatus;
    queue.push(i);
  };

  // Fill queue with seed nodes (or {0} if seed node container is empty)
  for(size_type i : seedNodes)
    queueNode(i);
  if (queue.empty())
    queueNode(0);

  // A temporary data structure to store the colors of already
  // colored neighbors.
  //
  // There's various possible data structures here. The most
  // obvious would be a set-like container storing the used
  // color indices. This could be (with increasing performance)
  // std::set, std::unordered_set, std::vector (keep sorted and unique),
  // std::vector (just append, sort and remove duplicates afterwards).
  // Since we expect that the total number of colors is small,
  // it is significantly faster to simply mark used colors in
  // a flag vector. Because memory usage is not an issue here,
  // we use std::vector<char> which measurably outperformes
  // std::vector<bool> due to faster bit access.
  std::vector<char> colorIsUsed;
  colorIsUsed.resize(32);

  // If the graph has several connected components that are not
  // reachable by the seed nodes, then we need to do several sweeps
  // of the advanving front method.
  while (not queue.empty())
  {
    // Use advancing front method to traverse graph
    while (not queue.empty())
    {
      // Process next entry from queue
      auto i = queue.front();
      queue.pop();

      // Collect colors of already colored neighbors,
      // put nonprocessed neighbors into queue,
      // and do nothing with already queued neighbors.
      colorIsUsed.assign(colorIsUsed.size(), false);
      for (const auto& neighbor: neighbors(i))
      {
        if (isColored(neighbor))
          colorIsUsed[color[neighbor]] = true;
        else if (isNonProcessed(neighbor))
          queueNode(neighbor);
      }

      // Assign first color that is not used by any processed neighbor to the current node
      color[i] = std::find(colorIsUsed.begin(), colorIsUsed.end(), false) - colorIsUsed.begin();

      // If necessary: Resize flag vector
      if (color[i]==colorIsUsed.size())
        colorIsUsed.resize(color[i]+1);
    }

    // If there are still some uncolored nodes, then the graph has connected
    // components that are not reachable by the provided seed nodes. Hence we
    // do another advancing front sweep starting from the first uncolored node.
    auto firstNonProcessedIt = std::find(color.begin(), color.end(), nonProcessedStatus);
    if (firstNonProcessedIt!=color.end())
      queueNode(firstNonProcessedIt - color.begin());
  }

  return color;
}



/**
 * \brief Compute a graph coloring from neighboring information
 *
 * This computes a coloring for a set of nodes with given neighboring relation
 * using a simple greedy advancing front algorithm.
 *
 * This overload allows to additionally pass a container of seed nodes
 * using a braced initializer list. It stores the entries in a std::vector
 * and forwards this to the overload with templated SeedNodesContainer.
 *
 * \param neighbors A callback that maps each node to the range of its neighbors
 * \param size Number of nodes in the graph
 * \param seedNodes std::initializer_list with seed nodes to be used for advancing front method.
 *
 * \returns A std::vector<size_type> containing the color of each node index
 */
template <class NeighborMap, class T>
auto computeColoring(const NeighborMap& neighbors, std::size_t size, std::initializer_list<T> seedIndices)
{
  return computeColoring(neighbors, size, std::vector<T>{seedIndices});
}



/**
 * \brief Compute a graph coloring from neighboring information
 *
 * This computes a coloring for a set of nodes with given neighboring relation
 * using a simple greedy advancing front algorithm.
 *
 * The input graph is represented as follows: Each node corresponds to an index.
 * These indices are zero-based, consecutive, and stored as size_type.
 * For every node i from 0,...,size-1 its neighbors are provided as a range
 * of indices by the callback neighbors(i).
 * The index type size_type is deduced from the entry type of neighbors(0).
 *
 * The algorithm initializes a queue with the seed node 0.
 * In an advancing front sweep the algorithm processes entries from the queue
 * until it is empty. Each entry is colored with the next free color not used
 * by any of it's neighbors, followed by adding all non-processed neighbors
 * to the queue.
 *
 * If the queue is empty, the first non-processed node is searched and added
 * to the queue to start the next sweep. This is done until there are no
 * non-processed nodes left.
 *
 * The number of needed sweeps corresponds to the number of connected components
 * in the graph. If there are many connected components the search for non-processed
 * noded can be avoided, by passing a container with seed nodes as additional argument.
 *
 * \param neighbors A callback that maps each node to the range of its neighbors
 * \param size Number of nodes in the graph
 *
 * \returns A std::vector<size_type> containing the color of each node index
 */
template <class NeighborMap>
auto computeColoring(const NeighborMap& neighbors, std::size_t size)
{
  return computeColoring(neighbors, size, {0});
}



/**
 * \brief Compute a vertex based neighbor map
 *
 * This identifes each element with its index in terms of a
 * MultipleCodimMultipleGeomTypeMapper(..., mcmgElementLayout())
 * and returns the corresponding neighbor map.
 * Elements are considered to be neighbors if they share a vertex.
 * Notice that this automatically includes the case that elements
 * share a subentity of any codimension.
 *
 * The produced neighbor map corresponds to a graph where grid elements
 * are the nodes and grid vertices are edges between adjacent
 * elements. This is similar to, but not identical to the dual graph
 * of the grid, because the latter only considers elements with
 * shared facets as neighbors.
 *
 * If the verbose mode is enabled, this computes and prints out
 * the number of the largest found clique in the element graph
 * which is a lower bound for the chromatic number of the graph
 * (i.e. the minimal number of colores required in a coloring).
 *
 * \tparam size_type Type used to store indices (default=uint32_t)
 * \param verbose Flag to enable verbose mode (default=false)
 */
template <class GridView, class size_type=uint32_t>
auto vertexNeighborMap(const GridView& gridView, bool verbose=false)
{
  static const auto dim = GridView::dimension;

  auto elementMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>(gridView, mcmgElementLayout());
  auto subEntityMapper = Dune::SingleCodimSingleGeomTypeMapper<GridView, dim>(gridView);

  // Compute a map of vertex index to vector of adjacent elements indices.
  // To improve performance we first precompute the sizes and preallocate
  // the local vectors.
  std::vector<std::vector<size_type>> adjacentElements(subEntityMapper.size());
  {
    // Precompute sizes
    std::vector<size_type> adjacentElementCount(adjacentElements.size(), 0);
    for (const auto& element : elements(gridView))
      for (std::size_t i = 0; i<element.subEntities(dim); ++i)
        ++adjacentElementCount[subEntityMapper.subIndex(element, i, dim)];
    // Preallocate
    for(auto i : Dune::range(adjacentElements.size()))
      adjacentElements[i].reserve(adjacentElementCount[i]);
  }
  for (const auto& element : elements(gridView))
  {
    auto elementIndex = elementMapper.index(element);
    for (std::size_t i = 0; i<element.subEntities(dim); ++i)
      adjacentElements[subEntityMapper.subIndex(element, i, dim)].push_back(elementIndex);
  }

  // In verbode mode: Compute maximal number of elements that share a vertex.
  // Elements sharing a vertex form a clique in the element graph. The largest found
  // clique is a lower bound for the clique number (largest existing clique)
  // which itself is a lower bound for the chromatic number.
  // Notice that even for planar 2d grids the element graph is not planar
  // and thus we may need more than 4 colors.
  if (verbose)
  {
    size_type maxCliqueSize = 0;
    for(const auto& clique : adjacentElements)
      maxCliqueSize = std::max(size_type(maxCliqueSize), size_type(clique.size()));
    std::cout << "Largest found element clique (lower bound of required colors) has size " << maxCliqueSize << std::endl;
  }

  // Compute a map of element index to vector of neighboring element indices.
  // Neighboring here means, that elements share a vertex.
  // To improve performance we first precompute the sizes and preallocate
  // the local vectors.
  std::vector<std::vector<size_type>> neighbors(elementMapper.size());
  {
    // Precompute upper estimate for sizes
    std::vector<size_type> neighborsCount(neighbors.size(), 0);
    for (const auto& vertexAdjacentElements : adjacentElements)
      for (auto k : vertexAdjacentElements)
        neighborsCount[k] += vertexAdjacentElements.size()-1;
    // Preallocate
    for(auto i : Dune::range(neighbors.size()))
      neighbors[i].reserve(neighborsCount[i]);
  }
  for (auto& vertexAdjacentElements : adjacentElements)
  {
    // Theoretically we could use a std::set for each neighbors[k]
    // and simply insert all entries from vertexAdjacentElements
    // into this set. However, simply pushing back entries and
    // removing duplicates afterwards is faster.
    for (auto k : vertexAdjacentElements)
      for (auto l : vertexAdjacentElements)
        if (k!=l)
          neighbors[k].push_back(l);
  }

  // Remove duplicate entries by first sorting them and then using std:::unique
  std::for_each(std::execution::par, neighbors.begin(), neighbors.end(), [&](auto&& elementNeighbors)
  {
    std::sort(elementNeighbors.begin(), elementNeighbors.end());
    auto lastUnique = std::unique(elementNeighbors.begin(), elementNeighbors.end());
    elementNeighbors.erase(lastUnique, elementNeighbors.end());
  });

  // Export result as a function
  return [neighbors=std::move(neighbors)](auto elementIndex) {
    return neighbors[elementIndex];
  };
}



/**
 * \brief Compute a layout based neighbor map
 *
 * This identifes each element with its index in terms of a
 * MultipleCodimMultipleGeomTypeMapper(..., mcmgElementLayout())
 * and returns the corresponding neighbor map.
 * Elements are considered to be neighbors if they share a subentity
 * that is enabled by the given layout callback. The callback models
 * the interface of the MultipleCodimMultipleGeomTypeMapper.
 *
 * \tparam size_type Type used to store indices (default=uint32_t)
 */
template <class GridView, class SubEntityLayout, class size_type=uint32_t>
auto subEntityNeighborMap(const GridView& gridView, const SubEntityLayout& subEntityLayout)
{
  static const auto dim = GridView::dimension;

  auto elementMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>(gridView, mcmgElementLayout());
  auto subEntityMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>(gridView, subEntityLayout);

  std::vector<std::vector<size_type>> neighbors(elementMapper.size());

  std::vector<std::vector<size_type>> adjacentElements(subEntityMapper.size());
  for (const auto& element : elements(gridView))
  {
    auto elementIndex = elementMapper.index(element);
    auto re = Dune::referenceElement<double,dim>(element.type());
    for (std::size_t codim = 1; codim <= dim; ++codim)
      for (std::size_t i = 0; i<re.size(codim); ++i)
        if (subEntityLayout(re.type(i, codim), GridView::dimension))
          adjacentElements[subEntityMapper.subIndex(element, i, codim)].push_back(elementIndex);
  }

  for (const auto& subEntityAdjacentElements : adjacentElements)
    for (auto k : subEntityAdjacentElements)
      for (auto l : subEntityAdjacentElements)
        if (k!=l)
          neighbors[k].push_back(l);

  for (auto& elementNeighbors : neighbors)
  {
    // Sort list of neighbors and make its entries unique.
    std::sort(elementNeighbors.begin(), elementNeighbors.end());
    auto lastUnique = std::unique(elementNeighbors.begin(), elementNeighbors.end());
    elementNeighbors.erase(lastUnique, elementNeighbors.end());
  }

  return [neighbors=std::move(neighbors)](auto elementIndex) {
    return neighbors[elementIndex];
  };
}



/**
 * \brief Compute a colored partition of the elements in the gridView from coloring
 *
 * This computes a vector containing one element range per color.
 * Each element range is a range all elements from the gridView that share the
 * respective color. Hence the outer size is the number of used colors, while the
 * sum of the inner sizes is the total number of elements in the gridView.
 * Internally each element range is stored as std::vector<EntitySeed> and stored
 * EntitySeeds are transformed to Entities on the fly during traversal.
 *
 * \param gridView GridView whose elements should be distributed
 * \param coloring A std::vector<size_type> representing a coloring as provided by computeColoring()
 *
 * \returns Partition of elements into chucks represented by std::vector<ElementRange>
 */
template<class GridView, class size_type>
auto coloredGridViewPartition(const GridView& gridView, const std::vector<size_type> coloring)
{
  size_type colorCount = *std::max_element(coloring.begin(), coloring.end()) + 1;

  using ElementSeed = typename GridView::Grid::template Codim<0>::EntitySeed;
  using ElementSeedRange = std::vector<ElementSeed>;

  std::vector<ElementSeedRange> elementSeedPartition(colorCount);
  auto elementMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>(gridView, mcmgElementLayout());
  for (const auto& element : elements(gridView))
    elementSeedPartition[coloring[elementMapper.index(element)]].push_back(element.seed());

  // For convenience transform from range of seeds to range of entities
  // This is save because transformedRangeView() stores copies of passed
  // r-values. Furthermore it transforms random access ranges to random
  // access ranges, such that the resulting entity range can be split
  // cheaply.
  auto seedToElement = [gridPtr = &gridView.grid()] (const auto& seed) {
    return gridPtr->entity(seed);
  };
  return Dune::transformedRangeView(std::move(elementSeedPartition), [seedToElement](const auto& seedRange) {
    return Dune::transformedRangeView(seedRange, seedToElement);
  });
}



/**
 * \brief Compute a colored partition of the elements in the gridView
 *
 * This is a short cut that subsequently computes a vertex based neighbor map
 * for the elementsin the grid view, a coloring for the induced element graph,
 * and finally the resulting colored element partition of the grid view.
 * The exact return type is explained in the documentation of coloredGridViewPartition().
 * If verbose mode is used, timing of each operation are printed to stdout.
 *
 * \tparam size_type Type used to store indices (default=uint32_t)
 *
 * \param gridView The grid view to partition
 * \param verbose Flag to enable verbose mode (default=false)
 *
 * \returns Partition of elements into chucks represented by std::vector<ElementRange>
 */
template<class GridView, class size_type=uint32_t>
auto coloredGridViewPartition(const GridView& gridView, bool verbose=false)
{
  Dune::Timer timer;

  timer.reset();
  auto adjacency = Dune::Fufem::vertexNeighborMap(gridView, verbose);
  if (verbose)
    std::cout << "Building adjacency map took " << timer.elapsed() << "s." << std::endl;

  timer.reset();
  auto coloring = Dune::Fufem::computeColoring(adjacency, gridView.size(0));
  if (verbose)
    std::cout << "Building coloring took " << timer.elapsed() << "s." << std::endl;

  timer.reset();
  auto elementPartition = Dune::Fufem::coloredGridViewPartition(gridView, coloring);
  if (verbose)
  {
    std::cout << "Building colored element partition took " << timer.elapsed() << "s." << std::endl;
    std::cout << "Number of used colors is " << elementPartition.size() << std::endl;
    std::size_t elementCount = 0;
    for(const auto& color : elementPartition)
      elementCount += color.size();
    std::cout << "Total number of elements in the partition is " << elementCount << std::endl;
  }

  return elementPartition;
}



/**
 * \brief Check if given element range is overlap free.
 *
 * The function exists for debunging purposes: It checks
 * if the given elementRange is overlap free in the sense
 * that for any pair of elements in the range, the set
 * of associated global indices in the given basis is disjoint.
 * The function will print out diagnostic information
 * if an overlap is detected.
 *
 * \returns True is no overlap is detected, otherwise false.
 */
template<class ElementRange, class Basis>
bool checkOverlapFree(const ElementRange& elementRange, const Basis& basis) {

  bool overlapFree = true;

  auto elementMapper = Dune::MultipleCodimMultipleGeomTypeMapper<typename Basis::GridView>(basis.gridView(), mcmgElementLayout());

  using MI = typename Basis::MultiIndex;
  std::unordered_map<MI, std::size_t> processedDOFs;

  auto localView = basis.localView();
  for (const auto& element : elementRange)
  {
    localView.bind(element);

    // Check if any of the DOFs of the present element showed up earlier.
    for (size_t i=0; i<localView.tree().size(); ++i)
    {
      auto localIndex = localView.tree().localIndex(i);
      auto globalIndex = localView.index(localIndex);
      auto it = processedDOFs.find(globalIndex);
      if (it != processedDOFs.end())
      {
        std::cout << "Overlap detected! Index [" << globalIndex << "] shows up in element "
          << it->second << " and " << elementMapper.index(element) << std::endl;
        overlapFree =false;
      }
    }

    for (size_t i=0; i<localView.tree().size(); ++i)
    {
      auto localIndex = localView.tree().localIndex(i);
      auto globalIndex = localView.index(localIndex);
      processedDOFs[globalIndex] = elementMapper.index(element);
    }
  }
  return overlapFree;
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_PARALLEL_ELEMENTCOLORING_HH
