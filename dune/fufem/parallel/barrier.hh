// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_PARALLEL_BARRIER_HH
#define DUNE_FUFEM_PARALLEL_BARRIER_HH

#include <cstddef>
#include <mutex>
#include <condition_variable>



namespace Dune {
namespace Fufem {


/**
 * \brief A barrier primitive for multi-threaded code.
 *
 * This implements a subset of C++20's std::barrier interface
 * which provides a simple synchronization barrier.
 * It's created with a counter. Each thread calling
 * arrive_and_wait() will decrement the counter and wait
 * until it reached zero. Once this has happened, the counter
 * is reset for the next phase.
 */
class Barrier {
public:

  Barrier(std::size_t count):
    safedCount_(count), remaining_(count), phase_(0)
  {}

  void arrive_and_wait()
  {
    // Acquire lock for writing to shared counter remaining_.
    auto l = std::unique_lock(mut);
    --remaining_;
    if (remaining_ != 0)
    {
      // If we're not the last thread to arrive:
      // Wait until all thread have been notified by the last one.
      // To avoid lost or spurious wake-ups one should always use
      // a condition.
      auto myphase = phase_+1;
      cv.wait(l, [&]{ return myphase <= phase_; });
    }
    else
    {
      // If we're the last thread to arrive:
      // Reset counter, proceed to next phase to provide a checkable condition
      // for the other threads and notify them that we're done.
      remaining_ = safedCount_;
      ++phase_;
      cv.notify_all();
    }
  }

private:
  mutable std::mutex mut;
  std::condition_variable cv;
  std::size_t safedCount_;
  std::ptrdiff_t remaining_;
  std::ptrdiff_t phase_ = 0;
};



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_PARALLEL_BARRIER_HH

