// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef VIRTUAL_DIFFERENTIABLE_FUNCTION_HH
#define VIRTUAL_DIFFERENTIABLE_FUNCTION_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

//! Dummy for DerivativeTypefier
struct DerivativeTypeNotImplemented
{};

//! Traits class for the type of derivative of a VirtualDifferentiableFunction
template <class DomainType, class RangeType>
struct DerivativeTypefier
{
  typedef DerivativeTypeNotImplemented DerivativeType;
};

template <class dctype, int DimDomain>
struct DerivativeTypefier<Dune::FieldVector<dctype, DimDomain>, double >
{
  typedef Dune::FieldMatrix<double, 1, DimDomain> DerivativeType;
};

template <class dctype, int DimDomain, class rctype, int DimRange>
struct DerivativeTypefier<Dune::FieldVector<dctype, DimDomain>, Dune::FieldVector<rctype, DimRange> >
{
  typedef Dune::FieldMatrix<rctype, DimRange, DimDomain> DerivativeType;
};

/**
 * \brief Virtual base class template for differentiable function
 *
 * \tparam DType The type of the input variable is 'const DType &'
 *
 * \tparam RType The type of the output variable is 'RType &'
 */
template <class DType, class RType>
class VirtualDifferentiableFunction
{
  public:
    typedef DType DomainType;
    typedef RType RangeType;
    typedef typename DerivativeTypefier<DomainType,RangeType>::DerivativeType DerivativeType;

    virtual ~VirtualDifferentiableFunction() {}

    /** \brief evaluation of 1st derivative
     *
     *  \param x point at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const = 0;


}; // end of VirtualDifferentiableFunction class



#endif
