// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_MAPPEDMATRIX_HH
#define DUNE_FUFEM_MAPPEDMATRIX_HH

#include <dune/common/iteratorfacades.hh>

#include <dune/istl/matrixutils.hh>


/**
 * \brief Iterator over sparse row of MappedMatrix
 *
 * Be careful: Since MappedMatrix is not a real container
 * the entries can only be accessed as copies. Hence the
 * exported type Reference is not a Value&.
 *
 * As a side effect 'operator->' cannot be used because
 * Dune::ForwardIteratorFacade does not allow to specify
 * a proxy class as Pointer type. Instead it tries to
 * get an address of the result of 'operator*'.
 */
template<class Map>
class MappedMatrixColIterator :
    public Dune::ForwardIteratorFacade<
        MappedMatrixColIterator<Map>,
        typename Map::block_type,
        const typename Map::block_type>

{
    typedef typename Map::Matrix Matrix;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;

public:
    typedef typename Map::block_type Value;
    typedef typename Map::block_type Reference;

    /** \brief Construct iterator that points at a specific column
     *
     * \param it Iterator pointing to the corresponding column of the untransformed matrix
     * \param virtualCol Column number in the set of columns that resulted from transforming the column pointed to by `it`.
     */
    MappedMatrixColIterator(const Map& map, typename Matrix::row_type::ConstIterator&& it, int row, int virtualRow, int virtualCol=0):
        map_(&map),
        it_(it),
        row_(row),
        virtualRow_(virtualRow),
        virtualCol_(virtualCol)
    {}

    void increment()
    {
        if (virtualCol_<colFactor-1)
        {
            ++virtualCol_;
        }
        else
        {
            virtualCol_ = 0;
            ++it_;
        }
    }

    bool equals(const MappedMatrixColIterator& other) const
    {
        return (it_==other.it_) and (virtualRow_==other.virtualRow_) and (virtualCol_==other.virtualCol_);
    }

    const Reference dereference() const
    {
        return map_->apply(*it_, row_, it_.index(), virtualRow_, virtualCol_);
    }

    int index() const
    {
        return it_.index()*colFactor+virtualCol_;
    }


protected:
    const Map* map_;
    typename Matrix::row_type::ConstIterator it_;
    const int row_;
    int virtualRow_;
    int virtualCol_;
};



/**
 * \brief Sparse row of MappedMatrix
 *
 * Be careful: Since MappedMatrix is not a real container
 * the rows are proxy objects returned as copies and not
 * as references.
 */
template<class Map>
class MappedMatrixRow
{
    typedef typename Map::Matrix Matrix;
    using size_type = typename Matrix::size_type;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;
public:
    typedef MappedMatrixColIterator<Map> ConstIterator;

    MappedMatrixRow(const Map& map, const Matrix& matrix, int row, int virtualRow):
        map_(&map),
        matrix_(&matrix),
        row_(row),
        virtualRow_(virtualRow)
    {}

    ConstIterator begin() const
    {
        return ConstIterator(*map_, (*matrix_)[row_].begin(), row_, virtualRow_);
    }

    ConstIterator end() const
    {
        return ConstIterator(*map_, (*matrix_)[row_].end(), row_, virtualRow_);
    }

    /** \brief The number of nonzero blocks in this row
     */
    size_type size() const
    {
        return (*matrix_)[row_].size() * colFactor;
    }

    /** \brief Get iterator to a particular column
     */
    ConstIterator find(typename Matrix::size_type col) const
    {
        auto originalEntry = (*matrix_)[row_].find(col/colFactor);
        return (originalEntry==(*matrix_)[row_].end())
            ? end()
            : ConstIterator(*map_, (*matrix_)[row_].find(col/colFactor), row_, virtualRow_, col % colFactor);
    }

protected:
    const Map* map_;
    const Matrix* matrix_;
    int row_;
    int virtualRow_;
};

namespace Dune
{
  // Forward declaration, so MatrixDimension can be friend of MappedMatrix
  template<typename M>
  struct MatrixDimension;
}


/**
 * \brief A sparse matrix built by entry-wise transformation
 *
 * MappedMatrix represents a sparse matrix that is obtained
 * by transforming each entry of some given matrix. The
 * transformed entries are not stored but computed on the fly
 * when dereferencing the corresponding iterators. The given
 * map transforms each entry into rowFactor x colFactor entries
 * that may also have a different block type.
 *
 * \tparam Map Type of transformation map
 *
 * The type Map must implement the following interface
 * \code{.cpp}
 * class MatrixMap
 * {
 * public:
 *     static const int rowFactor; // number of rows will be this times larger
 *     static const int colFactor; // number of columns will be this times larger
 * 
 *     typedef [...] Matrix; // type of matrix to be transformed
 * 
 *     typedef [...] block_type; // type of entries for transformed matrix 
 * 
 *     // entry transformation
 *     // transform (virtualRow,virtualCol)-th copy of (row,col)-th entry A original matrix 
 *     block_type apply(const typename Matrix::block_type& A, int row, int col, int virtualRow, int virtualCol) const;
 * };
 * \endcode
 *
 * Example use cases:
 * With rowFactor=colFactor=1 one can e.g. map each entry of the original matrix
 * to its local matrix norm. Conversely one can map scalar entries to a local matrices.
 * With rowFactor,colFactor>1 one can e.g. duplicate rows or columns.
 */
template<class Map>
class MappedMatrix
{
    template <typename M>
    friend class MatrixDimension;

    typedef typename Map::Matrix Matrix;
    static const int rowFactor = Map::rowFactor;
    static const int colFactor = Map::colFactor;

public:
    using size_type = typename Matrix::size_type;
    typedef MappedMatrixRow<Map> row_type;
    typedef typename Map::block_type block_type;
    typedef MappedMatrixColIterator<Map> ConstColIterator;

    //! %Iterator access to matrix rows
    template<class T>
    class RealRowIterator
      : public Dune::RandomAccessIteratorFacade<RealRowIterator<T>, T, const T>
    {
    public:
      //! \brief The unqualified value type
      typedef typename std::remove_const<T>::type ValueType;

      //! constructor
      RealRowIterator (const Map& map, const Matrix& matrix, typename Matrix::const_iterator originalRow)
        : map_(&map), matrix_(&matrix), originalRow_(originalRow), virtualRow_(0)
      {}

      /** \brief Default constructor, does not initialize anything
       */
      RealRowIterator () = default;

      //! return index
      size_type index () const
      {
        return originalRow_.index() * rowFactor + virtualRow_;
      }

      std::ptrdiff_t distanceTo(const RealRowIterator<const ValueType>& other) const
      {
        return originalRow_.distanceTo(other.originalRow_) * rowFactor + other.virtualRow_ - virtualRow_;
      }

      /** \brief Test two iterators for equality
       */
      bool equals (const RealRowIterator<const ValueType>& other) const
      {
        return originalRow_==other.originalRow_ and virtualRow_==other.virtualRow_;
      }

      //! prefix increment
      void increment()
      {
        ++virtualRow_;
        if (virtualRow_ == rowFactor)
        {
          ++ originalRow_;
          virtualRow_ = 0;
        }
      }

      //! prefix decrement
      void decrement()
      {
        if (virtualRow_ == 0)
        {
          virtualRow_ = rowFactor - 1;
          --originalRow_;
        }
        else
          --virtualRow_;
      }

      void advance(std::ptrdiff_t diff)
      {
        originalRow_ += diff/rowFactor;

        if (diff > 0)
          for (std::ptrdiff_t i=0; i<diff%rowFactor; ++i)
            increment();
        else
          for (std::ptrdiff_t i=0; i<-diff%rowFactor; ++i)
            decrement();
      }

      row_type elementAt(std::ptrdiff_t diff) const
      {
        auto newOriginalRow = originalRow_.index() + diff/rowFactor;
        auto newVirtualRow = (virtualRow_ + diff) % rowFactor;
        return MappedMatrixRow<Map>(*map_, *matrix_, newOriginalRow, newVirtualRow);
      }

      //! dereferencing
      row_type dereference () const
      {
        return MappedMatrixRow<Map>(*map_, *matrix_, originalRow_.index(), virtualRow_);
      }

      const Map* map_;
      const Matrix* matrix_;
      typename Matrix::const_iterator originalRow_;
      size_type virtualRow_;
    };

    //! The const iterator over the matrix rows
    using const_iterator = RealRowIterator<const row_type>;

    MappedMatrix(const Map& map, const Matrix& matrix):
        map_(&map),
        matrix_(&matrix)
    {}

    //! Get const iterator to first row
    auto begin () const
    {
      return RealRowIterator<const row_type>(*map_, *matrix_, matrix_->begin());
    }

    //! Get const iterator to one beyond last row
    auto end () const
    {
      return const_iterator(*map_, *matrix_, matrix_->end());
    }

    /** \brief Number of matrix rows
     */
    size_type N() const
    {
        return matrix_->N()*rowFactor;
    }

    /** \brief Number of matrix columns
     */
    size_type M() const
    {
        return matrix_->M()*colFactor;
    }

    const row_type operator[](int row) const
    {
        return MappedMatrixRow<Map>(*map_, *matrix_, row/rowFactor, row % rowFactor);
    }

    template<class X , class Y >
    void mv(const X &x, Y &y) const
    {
        int rows = this->N();
        y.resize(rows);
        for(int i=0; i<rows; ++i)
        {
            y[i] = 0.0;
            const row_type& Mi = (*this)[i];
            auto it = Mi.begin();
            auto end =  Mi.end();
            for(; it!=end; ++it)
                (*it).umv(x[it.index()], y[i]);
        }
    }

protected:
    const Map* map_;
    const Matrix* matrix_;
};

namespace Dune
{
  /** \brief Struct providing dimensions of the mapped matrix
   *
   * In particular, this is need to print mapped matrices using the `printmatrix` method
   * in `dune/istl/io.hh`.
   */
  template<typename Map>
  struct MatrixDimension<MappedMatrix<Map> >
  {
    using Matrix = MappedMatrix<Map>;
    using size_type = typename Matrix::size_type;

    static size_type rowdim (const Matrix& /*A*/, size_type /*i*/)
    {
      return Matrix::block_type::rows;
    }

    static size_type coldim (const Matrix& /*A*/, size_type /*c*/)
    {
      return Matrix::block_type::cols;
    }

    static size_type rowdim (const Matrix& A) {
      return A.N()*Matrix::block_type::rows;
    }

    static size_type coldim (const Matrix& A) {
      return A.M()*Matrix::block_type::cols;
    }
  };

}  // namespace Dune


#endif // DUNE_FUFEM_MAPPEDMATRIX_HH
