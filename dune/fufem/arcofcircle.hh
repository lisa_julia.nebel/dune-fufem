#ifndef ARC_OF_CIRCLE_HH
#define ARC_OF_CIRCLE_HH


#include <dune/grid/common/boundarysegment.hh>

class ArcOfCircle : public Dune::BoundarySegment<2>
{
    public:

    ArcOfCircle(const Dune::FieldVector<double,2>& center, double radius,
                double fromAngle, double toAngle)
    : center_(center), radius_(radius), fromAngle_(fromAngle), toAngle_(toAngle)
    {}

    Dune::FieldVector<double,2> operator()(const Dune::FieldVector<double,1>& local) const {

        double angle = fromAngle_ + local[0]*(toAngle_ - fromAngle_);

        Dune::FieldVector<double,2> result = center_;
        result[0] += radius_ * std::cos(angle);
        result[1] += radius_ * std::sin(angle);

        return result;
    }

    Dune::FieldVector<double,2> center_;
    
    double radius_;

    double fromAngle_;

    double toAngle_;
};

/** \brief Boundary segments for a 3D-tube/tube-segment prism grid that is aligned with the x-axis.
 *
 *  \param center - The center of the circle that the characterizes the position of tube.
 *  \param radius - The radius of the circle.
 *  \param length - Length of the boundary segment in x-direction.
 *  \param fromAngle - Segment fromAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *  \param toAngle - Segment toAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *
 */
template<class ctype>
class TubeLowerSegment : public Dune::BoundarySegment<3>
{
    public:

    TubeLowerSegment(const Dune::FieldVector<ctype,3>& center, ctype radius, ctype length,
                ctype fromAngle, ctype toAngle)
    : center_(center), radius_(radius), length_(length), fromAngle_(fromAngle), toAngle_(toAngle)
    {}

    Dune::FieldVector<ctype,3> operator()(const Dune::FieldVector<ctype,2>& local) const {

        ctype angle = fromAngle_ + (local[0] + local[1])*(toAngle_ - fromAngle_);

        Dune::FieldVector<ctype,3> result = center_;

        result[0] += local[1]*length_;
        result[1] += radius_ * std::cos(angle);
        result[2] += radius_ * std::sin(angle);

        return result;
    }

    Dune::FieldVector<ctype,3> center_;
    
    ctype radius_;
    ctype length_;

    ctype fromAngle_;
    ctype toAngle_;
};

/** \brief Boundary segments for a 3D-tube/tube-segment prism grid that is aligned with the x-axis.
 *
 *  \param center - The center of the circle that the characterizes the position of tube.
 *  \param radius - The radius of the circle.
 *  \param length - Length of the boundary segment in x-direction.
 *  \param fromAngle - Segment fromAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *  \param toAngle - Segment toAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *
 */
template<class ctype>
class TubeUpperSegment : public Dune::BoundarySegment<3>
{
    public:

    TubeUpperSegment(const Dune::FieldVector<ctype,3>& center, ctype radius, ctype length,
                ctype fromAngle, ctype toAngle)
    : center_(center), radius_(radius), length_(length), fromAngle_(fromAngle), toAngle_(toAngle)
    {}

    Dune::FieldVector<ctype,3> operator()(const Dune::FieldVector<ctype,2>& local) const {

        ctype angle = fromAngle_ + local[0]*(toAngle_ - fromAngle_);

        Dune::FieldVector<ctype,3> result = center_;

        result[0] += local[1]*length_;
        result[1] += radius_ * std::cos(angle);
        result[2] += radius_ * std::sin(angle);

        return result;
    }

    Dune::FieldVector<ctype,3> center_;
    
    ctype radius_;
    ctype length_;

    ctype fromAngle_;
    ctype toAngle_;
};

/** \brief Lateral boundary segments for a 3D-tube/tube-segment prism grid that is aligned with the x-axis.
 *
 *  \param center - The center of the circle that the characterizes the position of tube.
 *  \param radius - The radius of the circle.
 *  \param length - Thickness of the tube(-segment).
 *  \param fromAngle - Segment fromAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *  \param toAngle - Segment toAngle. (0 <= fromAngle <= toAngle <= 2*PI)
 *
 */
template<class ctype>
class TubeLateralSegment : public Dune::BoundarySegment<3>
{
    public:

    TubeLateralSegment(const Dune::FieldVector<ctype,3>& center, ctype radius, ctype thickness,
                ctype fromAngle, ctype toAngle)
    : center_(center), radius_(radius), thickness_(thickness), fromAngle_(fromAngle), toAngle_(toAngle)
    {}

    Dune::FieldVector<ctype,3> operator()(const Dune::FieldVector<ctype,2>& local) const {

        ctype angle = fromAngle_ + local[0]*(toAngle_ - fromAngle_);

        Dune::FieldVector<ctype,3> result = center_;

        result[1] += (radius_ +local[1]*thickness_)* std::cos(angle);
        result[2] += (radius_ +local[1]*thickness_)* std::sin(angle);

        return result;
    }

    Dune::FieldVector<ctype,3> center_;
    
    ctype radius_;
    ctype thickness_;

    ctype fromAngle_;
    ctype toAngle_;
};

#endif
